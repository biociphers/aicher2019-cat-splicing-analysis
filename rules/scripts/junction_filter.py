#!/usr/bin/env python
"""
junction_filter.py

Filters set of junctions using PSI values from a group of samples.

Inputs:
    - psi_files: Paths to tables of junctions with psi quantifications per
      junction for the given group of samples. Tab-separated with columns
      lsv_id, junction_id, psi, gene_id, gene_name, nonambiguous, constitutive
    - junction_file: Path to table of lsv/junctions (columns lsv_id,
      junction_id, gene_id, gene_name, nonambiguous, among others (matching
      others from psi_files (except psi))) from which filtering will be
      performed
Output:
    - tab-separated file of lsv_id/junction_id, information about the
      lsv/junction, and information about the filtering
Parameters:
    - min_psi: minimum value of PSI in samples that will be kept after
      filtering (adjusted for by pct_relaxation)
    - max_psi: maximum value of PSI in samples that will be kept after
      filtering (adjusted for by pct_relaxation)
    - pct_relaxation: relaxation on what percent of samples need to be
      above/below min_psi or max_psi, 1 corresponds to exact min/max

Note that some samples in the group may not be quantified for some lsv/junction
pair. For this project, we are intersted in the loss of functional junctions
that are included in non-CATs that are either lost or unable to be detected in
CATs. Thus, we treat unquantified samples as psi = 0 for the purposes of
filtering, although we emphasize that these situations are quite different.

Author: Joseph K Aicher
"""

import numpy as np
import pandas as pd
from functools import reduce


PCT_RELAXATION = 0.85


def main(
        psi_files, junction_file=None, min_psi=None, max_psi=None,
        pct_relaxation=PCT_RELAXATION
):
    """ Filters set of junctions using PSI values from a group of samples.

    Parameters
    ----------
    psi_files: List[str]
        Paths to tables of junctions with psi quantifications pre junction for
        the given group of samples
    junction_file: str
        Path to table of lsv/junctions (columns lsv_id, junction_id, gene_id,
        gene_name, constitutive)
    min_psi, max_psi: Optional[float]
        Thresholds for filtering on min/max psi (relaxed versions)
    pct_relaxation: float
        Percent of quantified samples used for min/max calculations

    Returns
    -------
    pd.DataFrame
        Indexed by lsv_id, junction_id
        Columns: ["psi_min", "psi_max", "num_quant", "pct_quant",
                  "constitutive", "nonambiguous", "gene_id", "gene_name"]
        summary over samples in this group of statistics filtered to meet
        criteria on extreme values of the population
    """
    # load junctions file for filtering against
    if junction_file:
        df_junctions = pd.read_csv(
            junction_file, sep="\t",
            usecols=[
                "lsv_id", "junction_id",
                "gene_id", "gene_name", "constitutive"
            ]
        ).set_index(["lsv_id", "junction_id"], verify_integrity=True)
    else:
        df_junctions = None
    # load the quantifications altogether
    df_psi = load_quantifications(psi_files, df_junctions=df_junctions)
    # summarize across samples for lsv/junctions
    df_summary = summarize_quantifications(
        df_psi, df_junctions=df_junctions, pct_relaxation=pct_relaxation,
        num_samples=len(psi_files)
    )
    # filter on results and be done
    return filter_summary(df_summary, min_psi=min_psi, max_psi=max_psi)


def filter_summary(df_summary, min_psi=None, max_psi=None):
    """ Applies filters on min/max psi to summary of lsv/junctions

    Parameters
    ----------
    df_summary: pd.DataFrame
        Indexed by ["lsv_id", "junction_id"]
        Columns: ["psi_min", "psi_max", "num_quant", "pct_quant",
                  "constitutive", "nonambiguous", "gene_id", "gene_name"]
    min_psi, max_psi: float
        thresholds on psi extremes from summary

    Returns
    -------
    pd.DataFrame
        filtered version of df_summary meeting specified criteria
    """
    # obtain filters
    result_filters = list()
    if min_psi is not None:
        result_filters.append(df_summary["psi_min"] >= min_psi)
    if max_psi is not None:
        result_filters.append(df_summary["psi_max"] <= max_psi)
    # apply obtained filters, if any
    if result_filters:
        df_summary = df_summary.loc[
            reduce((lambda x, y: x & y), result_filters)
        ]
    # return the result
    return df_summary


def summarize_quantifications(
        df_psi, df_junctions=None, pct_relaxation=PCT_RELAXATION,
        num_samples=None
):
    """ Summarize quantifications across samples (per lsv/junction)

    Parameters
    ----------
    df_psi: pd.DataFrame
        Combined quantifications in the group.
        Indexed by sample_ndx, lsv_id, junction_id
        Columns: [
            "psi", "constitutive", "gene_name", "gene_id"
        ]
    df_junctions: Optional[pd.DataFrame]
        If specified, used to process only lsv/junctions that are listed, with
        lsv/junctions unquantified in any of the samples being treated as if
        psi = 0.
        Indexed by lsv_id, junction_id
        Columns: [
            "psi", "constitutive", "gene_name", "gene_id"
        ]
    pct_relaxation: Optional[float]
        Relaxation on what percent of samples used for calculation of min/max
        psi values
    num_samples: Optional[int]
        The number of samples expected to be in df_psi. If not provided,
        inferred from levels of df_psi.index

    Returns
    -------
    pd.DataFrame
        Indexed by ["lsv_id", "junction_id"]
        Columns: ["psi_min", "psi_max", "num_quant", "pct_quant",
                  "constitutive", "gene_id", "gene_name"]
    """
    # infer num_samples if necessary
    if num_samples is None:
        num_samples = len(df_psi.index.levels[0])
    # group df_psi by lsv_id, junction_id
    grouped_df = df_psi.groupby(["lsv_id", "junction_id"])
    # get non-psi summarized values
    summary_notpsi = grouped_df.agg({
        # absolutely necessary
        "constitutive": "first",
        # would like but okay if need to relax this later
        "gene_name": "first",
        "gene_id": "first"
    })
    # get psi summarized values
    summary_psi = grouped_df["psi"].agg([
        ("num_quant", "count"),
        (
            "psi_min",
            lambda x: _padded_quantile(
                x, 1 - pct_relaxation, num_samples, 0., interpolation="lower"
            )
        ),
        (
            "psi_max",
            lambda x: _padded_quantile(
                x, pct_relaxation, num_samples, 0., interpolation="higher"
            )
        )
    ])
    summary_psi["pct_quant"] = summary_psi["num_quant"] / num_samples
    # get combined summary
    summary = summary_psi.join(summary_notpsi, how="inner")
    # add in remaining rows from df_junctions if present
    if df_junctions is not None:
        # obtain the missing values that we will add to the summary
        missing_values = df_junctions.loc[
            # lsv/junctions that are not currently in summary
            df_junctions.index.difference(summary.index)
        ]
        # set the quantifications appropriately (zeros)
        missing_values["num_quant"] = 0
        missing_values["pct_quant"] = 0.
        missing_values["psi_min"] = 0.
        missing_values["psi_max"] = 0.
        summary = pd.concat([summary, missing_values], join="inner")
    # return result
    return summary


def _padded_quantile(
        x, q, total_samples, padding_value, interpolation="linear"
):
    """ q-th quantile over total_samples including x with default padding value

    Obtains q-th quantile over total_samples observations. x must be no larger
    than total_samples, and the remaining samples are set as padding_value.

    Parameters
    ----------
    x: pd.Series(numeric)
        Values for which a padded quantile will be computed
    q: float
        Quantile to compute, which must be in [0, 1] inclusive
    total_samples: int
        x is considered among a total of these many observations. Note that
        len(x) <= total_samples is required.
    padding_value: float
        The observations not provided by x are set to this value
    interpolation: {'linear', 'lower', 'higher', 'midpoint', 'nearest'}
        Optional parameter describes the interpolation method to use when
        desired quantile lies between two datapoints. See `np.quantile` for
        details.

    Returns
    -------
    float (same length as q)
        The padded quantile
    """
    padded_observations = np.empty(total_samples)
    padded_observations[:] = padding_value  # set default value
    padded_observations[:len(x)] = x.values  # set the values of x in here
    return np.quantile(padded_observations, q, interpolation=interpolation)


def relaxed_psi_summary(grouped_psi, num_samples, pct_relaxation):
    """ Applied to grouped series of psi values to summarize over samples

    Obtains relaxed min/max of psi values, and the percentage/count quantified

    Parameters
    ----------
    grouped_psi: pd.Series
        Psi values we are summarizing
    num_samples: int
        Number of samples that could have been quantified
    pct_relaxation: float
        Relaxation on what percent of samples used for calculation of min/max
        psi values

    Returns
    -------
    pd.Series(object)
        series with keys: "psi_min", "psi_max", "pct_quant", "num_quant"
        corresponding to relaxed estimates of extreme values
    """
    # get the number/percent quantified
    num_quant = grouped_psi.count()
    pct_quant = num_quant / num_samples
    # get the minimum value
    psi_min = 0.  # default value
    if pct_relaxation <= pct_quant:  # relaxed value is quantified
        psi_min = grouped_psi.quantile(
            1 - pct_relaxation / pct_quant, interpolation="lower"
        )
    # get the maximum value
    psi_max = 0.  # default value
    if 1 - pct_relaxation <= pct_quant:  # relaxed value is quantified
        psi_max = grouped_psi.quantile(
            1 - ((1 - pct_relaxation) / pct_quant), interpolation="higher"
        )
    # return series with these values
    return pd.Series({
        "num_quant": num_quant,
        "pct_quant": pct_quant,
        "psi_min": psi_min,
        "psi_max": psi_max
    })


def load_quantifications(psi_files, df_junctions=None):
    """ Load and concatenate quantifications in the group

    Parameters
    ----------
    psi_files: List[str]
        Paths to tables of junctions with psi quantifications per junction for
        the given group of samples. Tab-separated with columns lsv_id,
        junction_id, psi, nonambiguous, among others
    df_junctions: Optional[pd.DataFrame]
        If specified, dataframe with multi-index lsv_id, junction_id used to
        filter psi_files to only process lsv/junctions that are listed

    Returns
    -------
    pd.DataFrame
        Indexed by sample_ndx, lsv_id, junction_id
        Columns as in psi_files, except for column nonambiguous
        Keeping only nonambiguous LSVs (ambiguous LSVs have been filtered out)
        Concatenation of the individual quantifications
    """
    # lazily load sample quantifications
    dfs_samples = (
        pd.read_csv(
            # load from file
            f, sep="\t"
        ).set_index(
            # set the index
            ["lsv_id", "junction_id"]
        ).loc[
            # only keep nonambiguous lsvs
            lambda df: df["nonambiguous"]
        ].drop(
            # remove nonambiguous column
            columns="nonambiguous"
        )
        for f in psi_files
    )
    # if df_junctions provided, lazily filter for matching lsv/junctions
    if df_junctions is not None:
        dfs_samples = (
            # keep only matching lsvs/junctions
            df.loc[df_junctions.index.intersection(df.index)]
            for df in dfs_samples
        )
    # concatenate the dataframes, extending the index to include sample details
    return pd.concat(
        dfs_samples,
        join="inner", keys=list(range(len(psi_files))), names=["sample_ndx"]
    )


if __name__ == "__main__":
    try:  # get all parameters from snakemake
        # inputs
        psi_files = snakemake.input.psi_files
        junction_file = snakemake.input.get("junction_file", None)
        # parameters
        min_psi = snakemake.params.get("min_psi", None)
        max_psi = snakemake.params.get("max_psi", None)
        pct_relaxation = snakemake.params.get("pct_relaxation", PCT_RELAXATION)
        # output
        output_tsv = snakemake.output[0]
    except NameError:  # not running snakemake, try command-line
        # set up argument parser
        import argparse
        from sys import stdout
        parser = argparse.ArgumentParser(
            description="Filters set of junctions using PSI values from group"
            " of samples"
        )
        parser.add_argument(
            "psi_files", type=str, nargs="+", help="Paths to tables of"
            " junctions with psi quantifications (expected columns are"
            " 'lsv_id', 'junction_id', 'psi', 'gene_id', 'gene_name',"
            " 'nonambiguous')"
        )
        parser.add_argument(
            "--junction-file", type=str, default=None,
            help="Paths to table of junctions to filter from with same fields"
            " as psi_files but without column psi or nonambiguous"
        )
        parser.add_argument("--output", type=argparse.FileType("w"),
                            default=stdout,
                            help="Where to write output (default stdout)")
        parser.add_argument("--min-psi", type=float, default=None,
                            help="If specified, minimum value of PSI in"
                            " samples that will be kept after filtering"
                            " (default: %(default)s)")
        parser.add_argument("--max-psi", type=float, default=None,
                            help="If specified, maximum value of PSI in"
                            " samples that will be kept after filtering"
                            " (default: %(default)s)")
        parser.add_argument("--pct-relaxation", type=float,
                            default=PCT_RELAXATION,
                            help="Percentage of samples min/max calculated on"
                            " (default: %(default)s)")
        # get arguments
        args = parser.parse_args()
        # inputs
        psi_files = args.psi_files
        junction_file = args.junction_file
        # parameters
        min_psi = args.min_psi
        max_psi = args.max_psi
        pct_relaxation = args.pct_relaxation
        # output
        output_tsv = args.output
    # run for resulting summary
    result = main(psi_files, junction_file, min_psi=min_psi, max_psi=max_psi,
                  pct_relaxation=pct_relaxation)
    # save result and done!
    result.to_csv(output_tsv, sep="\t", index=True)
