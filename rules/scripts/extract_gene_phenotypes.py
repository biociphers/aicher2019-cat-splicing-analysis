#!/usr/bin/env python
"""
extract_gene_phenotypes.py

Extract information from VCF-format variant annotations (e.g. ClinVar, HGMD)
about gene names and their relationship to phenotypes

Author: Joseph K Aicher
"""

import re  # for regular expressions
import cyvcf2  # for parsing VCF
from sys import stdout  # for output to stdout


# default parameters (for ClinVar)
GENE_FIELD = "GENEINFO"
GENE_PATTERN = "([^:]+):.+"
GENE_IGNORE = "|"
PATHOGENIC_FIELD = "CLNSIG"
PATHOGENIC_PATTERN = ".*[Pp]athogenic.*"
PHENOTYPE_FIELD = "CLNDN"
PHENOTYPE_PATTERN = "(.+)"
IGNORE_CLNREVSTAT = [
    "no_assertion_criteria_provided", "no_assertion_provided",
    "no_interpretation_for_the_single_variant", "_conflicting_interpretations"
]


def get_args():
    """ Get command-line arguments for the script

    Returns
    -------
    args: namespace with parsed command-line arguments
    """
    import argparse

    # create parser
    parser = argparse.ArgumentParser(
        description="Extracts from VCF-format variant annotations (i.e."
        " ClinVar, HGMD) gene-phenotype relationships under flexible filtering"
        " criteria"
    )

    # add required/optional arguments
    parser.add_argument("input_vcf", metavar="input-vcf", type=str,
                        help="Path to VCF with gene-phenotype relationships")
    parser.add_argument("--output-tsv", type=argparse.FileType(mode="w"),
                        default=stdout,
                        help="Path for output tsv with gene_name-phenotype"
                        " relationships (default: print to stdout)")
    parser.add_argument("--gene-field", type=str, default=GENE_FIELD,
                        help="field name in INFO containing gene name"
                        " (default: %(default)s)")
    parser.add_argument("--gene-pattern", type=str, default=GENE_PATTERN,
                        help="regex for capturing gene name in `gene-field`"
                        " (default: '%(default)s')")
    parser.add_argument("--gene-ignore", type=str, default=GENE_IGNORE,
                        help="string with characters that are not allowed in"
                        " gene field. Used to identify records with"
                        " delimiters, indicating ambiguous assignment. For"
                        " example, ClinVar uses `|` as a delimiter in GENEINFO"
                        " (default: %(default)s)")
    parser.add_argument("--pathogenic-field", type=str,
                        default=PATHOGENIC_FIELD,
                        help="field name in INFO containing pathogenicity"
                        " information (default: %(default)s")
    parser.add_argument("--pathogenic-pattern", type=str,
                        default=PATHOGENIC_PATTERN, help="regex for detecting"
                        " pathogenic event of interest in `pathogenic-field`"
                        " (default: '%(default)s')")
    parser.add_argument("--phenotype-field", type=str,
                        default=PHENOTYPE_FIELD,
                        help="field name in INFO containing phenotype"
                        " (default: %(default)s)")
    parser.add_argument("--phenotype-pattern", type=str,
                        default=PHENOTYPE_PATTERN,
                        help="regex to match/extract phenotype association"
                        " (default: '%(default)s')")
    parser.add_argument("--ignore-CLNREVSTAT", dest="ignore_clnrevstat",
                        type=str, nargs="+", default=IGNORE_CLNREVSTAT,
                        help="If CLNREVSTAT field exists, clinical review"
                        " statuses to ignore (for ClinVar)"
                        " (default: {%(default)s})")

    # parse arguments from command line
    return parser.parse_args()


def main(input_vcf, output_tsv=stdout, gene_field=GENE_FIELD,
         gene_pattern=GENE_PATTERN, gene_ignore=GENE_IGNORE,
         pathogenic_field=PATHOGENIC_FIELD,
         pathogenic_pattern=PATHOGENIC_PATTERN,
         phenotype_field=PHENOTYPE_FIELD,
         phenotype_pattern=PHENOTYPE_PATTERN,
         ignore_clnrevstat=IGNORE_CLNREVSTAT):
    """ Produce desired tabular output of gene/phenotype correlations from VCF

    Produce desired tabular output of gene/phenotype correlations from VCF,
    printing output to `output_tsv`

    Parameters
    ----------
    input_vcf: str
        Path to VCF with gene-phenotype relationships
    output_tsv: Optional[fileobj]
        File object for output tsv with gene_name-phenotype relationships
    gene_field: Optional[str]
        Field name in INFO containing gene name
    gene_pattern: Optional[str]
        Regex for capturing gene name in `gene_field`
    gene_ignore: Optional[str]
        Ignore entries where gene_field contains any of the characters in the
        input string
    pathogenic_field: Optional[str]
        Field name in INFO containing pathogenicity information
    pathogenic_pattern: Optional[str]
        Regex for detecting pathogenic event to use in `pathogenic_field`
    phenotype_field: Optional[str]
        Field name in INFO containing phenotype information
    phenotype_pattern: Optional[str]
        Regex for detecting/extracting phenotype information from entry
    ignore_clnrevstat: Optional[List[str]]
        When field CLNREVSTAT (e.g. ClinVar VCF) exists, ignore entries taking
        these values
    """
    # initialize dictionary matching gene names to sets of phenotypes
    gene_phenotypes = dict()
    # set up variables/compile regexes
    gene_ignore_list = list(gene_ignore)  # split into individual characters
    gene_re = re.compile(gene_pattern)
    pathogenic_re = re.compile(pathogenic_pattern)
    phenotype_re = re.compile(phenotype_pattern)
    # for each variant entry in the input VCF, see if we can add to it...
    for entry in cyvcf2.VCF(input_vcf):
        # CLNREVSTAT filter
        if entry.INFO.get("CLNREVSTAT") in ignore_clnrevstat:
            continue
        # gene_ignore filter
        gene_entry = entry.INFO.get(gene_field)
        try:
            if any(x in gene_entry for x in gene_ignore_list):
                continue
        except TypeError:  # gene_entry not iterable (None)
            continue
        # pathogenic filter
        pathogenic_entry = entry.INFO.get(pathogenic_field)
        if not pathogenic_re.fullmatch(pathogenic_entry):
            continue
        # if we're still here, we have a winner
        # extract gene
        gene_name = gene_re.match(gene_entry).group(1)
        # extract phenotypes
        phenotype = phenotype_re.match(
            entry.INFO.get(phenotype_field)
        ).group(1).split("|")  # get list delimited by |
        # update gene_phenotypes
        try:
            gene_phenotypes[gene_name].union(phenotype)
        except KeyError:
            gene_phenotypes[gene_name] = set(phenotype)
    # done parsing VCF
    # start writing output
    print("gene_name\tphenotypes", file=output_tsv)
    for gene_name in sorted(gene_phenotypes.keys()):
        phenotypes = ",".join(sorted(gene_phenotypes[gene_name]))
        print(f"{gene_name}\t{phenotypes}", file=output_tsv)
    return


if __name__ == "__main__":
    # get command-line arguments
    args = get_args()
    main(args.input_vcf, output_tsv=args.output_tsv,
         gene_field=args.gene_field, gene_pattern=args.gene_pattern,
         gene_ignore=args.gene_ignore, pathogenic_field=args.pathogenic_field,
         pathogenic_pattern=args.pathogenic_pattern,
         phenotype_field=args.phenotype_field,
         phenotype_pattern=args.phenotype_pattern,
         ignore_clnrevstat=args.ignore_clnrevstat)
