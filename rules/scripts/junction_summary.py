#!/usr/bin/env python
"""
junction_summary.py

Summarizes lsv/junction lists to gene lists and annotates with median TPM per
gene over provided samples

Author: Joseph K Aicher
"""

import pandas as pd


def junction_summary(junction_file, expression_file):
    """ Obtains table with summary of junctions as genes with expression info

    Parameters
    ----------
    junction_file: str
        Path for table with junctions to filter from with columns:
        ["junction_id", "gene_name", "constitutive"]
    expression_file: str
        Path for table with tissue gene expression values, with columns
        `gene_name` and `TPM_median`

    Returns
    -------
    pd.DataFrame
        Indexed by "gene_name"
        Columns "num_junctions", "num_constitutive", "TPM_median"
    """
    # load junctions and summarize gene information
    return pd.read_csv(
        # load tab-separated junction file
        junction_file, sep="\t"
    ).groupby(
        # summarize over junctions/genes (remove LSV construction from counts
        # of junctions satisfying criteria)
        ["gene_name", "junction_id"]
    ).agg({
        # constitutive shouldn't matter whether any or all
        "constitutive": "all",
    }).reset_index().groupby(
        # group by gene_name only now
        "gene_name"
    ).agg({
        # count the number of unique junction ids
        "junction_id": "count",
        "constitutive": "sum"
    }).rename(columns={
        # rename columns
        "junction_id": "num_junctions",
        "constitutive": "num_constitutive",
    }).join(
        pd.read_csv(
            expression_file, sep="\t", usecols=["gene_name", "TPM_median"]
        ).set_index("gene_name"),
        # only keep genes that we also had gene expression for
        # this was observed to discard a total of 20 genes (counting
        # duplicates) across all non-CATs
        how="inner"
    )


if __name__ == "__main__":
    try:
        # inputs
        junction_file = snakemake.input.junctions
        expression_file = snakemake.input.expression
        # output
        output_tsv = snakemake.output[0]
    except NameError:  # not running snakemake, try command-line
        # imports for when run as script
        import argparse
        from sys import stdout

        # set up argument parser
        parser = argparse.ArgumentParser(
            description="Summarizes junctions to genes and annotates with"
            " median abundances from input samples"
        )
        parser.add_argument(
            "junction_file", type=str,
            help="Path to table of junctions to summarize with columns"
            " `junction_id`, `gene_name`, `constitutive`"
        )
        parser.add_argument(
            "expression_file", type=str,
            help="Path to table of tissue gene expression with columns"
            " `gene_name` and `TPM_median`"
        )
        parser.add_argument(
            "--output", type=argparse.FileType("w"), default=stdout,
            help="Where to write output (default stdout)"
        )

        # get arguments
        args = parser.parse_args()
        # put in variables matching those from try block
        junction_file = args.junction_file
        expression_file = args.expression_file
        output_tsv = args.output
    # get junction summary
    result = junction_summary(junction_file, expression_file)

    # save result and done!
    result.to_csv(output_tsv, sep="\t", index=True)
