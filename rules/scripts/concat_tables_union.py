#!/usr/bin/env python
"""
concat_tables_union.py

Obtains a union of tables with intersection of columns. If provided, uses index
columns to do union on the basis of unique keys, ignoring other columns.

Author: Joseph K Aicher
"""

# imports
import pandas as pd  # we will use pandas dataframes


def tables_union(input_tables, index_columns=list(), drop_columns=list()):
    """ Obtains the union of table entries for columns present in all tables

    Parameters
    ----------
    input_tables: List[str or fileobj]
        List of paths/fileobjects for input tables
    index_columns: List[str]
        If provided, column labels to be used as index for identifying
        duplicate entries rather than all columns.
    drop_columns: List[str]
        If provided, columns that will be excluded from result

    Returns
    -------
    pd.DataFrame
        dataframe with union of entries
    """
    # load the dataframes with dropped columns
    dfs = load_tables(input_tables, drop_columns=drop_columns)
    # get union of these
    if not index_columns:
        # no index provided, so we do brute-force
        return pd.concat(
            dfs, join="inner", ignore_index=True
        ).drop_duplicates()
    # otherwise...
    return pd.concat(filter_index_union(dfs, index_columns), join="inner")


def filter_index_union(dfs, index_columns):
    """ Lazy iterator over dataframes to filter indexes previously seen before

    Parameters
    ----------
    dfs: Iterator[pd.DataFrame]
        Loaded tables including the columns `index_columns`
    index_col: List[str]
        Column labels to be used as index for identifying duplicate entries

    Returns
    -------
    Iterator[pd.DataFrame]
        Loaded tables, filtered for previously seen index values
    """
    # initialize empty index of previously seen values
    union_index = pd.Index([])
    # loop over dataframes and filter on previously seen indexes
    for df in dfs:
        df = df.set_index(
            # set index using provided columns
            index_columns
        ).loc[
            # keep only values not previously seen before
            lambda x: x.index.difference(union_index)
        ]
        # update union index
        union_index = union_index.union(df.index)
        # yield filtered dataframe
        yield df
    return


def load_tables(input_tables, drop_columns=list()):
    """ Lazy iterator to load input tables, keeping only relevant columns

    Parameters
    ----------
    input_tables: List[str or fileobj]
        List of paths/fileobjects for input tables
    drop_columns: List[str]
        If provided, columns that will be excluded from result

    Returns
    -------
    Iterator[pd.DataFrame]
        Loaded tables
    """
    # keep track of columns present in all the loaded dataframes so far
    column_intersection = None  # haven't loaded any yet
    for input_table in input_tables:
        # load this input table
        df = pd.read_csv(
            input_table, sep="\t",
            usecols=lambda x: (
                # x cannot be in drop columns
                (x not in drop_columns)
                and
                # x must be in the intersection of previously seen columns
                (
                    column_intersection is None  # True if none loaded yet
                    or
                    x in column_intersection
                )
            )
        )
        # update column_intersection (use current dataframe columns)
        column_intersection = df.columns
        # yield the loaded dataframe
        yield df
    return


if __name__ == "__main__":
    try:  # get all parameters from snakemake
        # inputs
        input_tables = snakemake.input
        # parameters
        index_columns = snakemake.params.get("index_columns", list())
        drop_columns = snakemake.params.get("drop_columns", list())
        # output
        output_tsv = snakemake.output[0]
        pass
    except NameError:  # not running snakemake, try command-line
        # set up parser
        import argparse
        from sys import stdout
        parser = argparse.ArgumentParser(
            description="Obtains a union of tables with intersection of"
            " columns. If provided use index columns to do union on basis of"
            " unique keys, ignoring unique information in other columns."
        )
        # set up arguments
        parser.add_argument(
            "input_tables", type=argparse.FileType(mode="r"), nargs="+",
            help="Input tables to concatenate the union of"
        )
        parser.add_argument(
            "--index-columns", type=str, nargs="*", default=list(),
            help="If provided, column names of unique index that will be used"
            " to search for duplicates over instead of all columns"
        )
        parser.add_argument(
            "--drop-columns", type=str, nargs="*", default=list(),
            help="If provided, column names to be dropped from output"
        )
        parser.add_argument(
            "--output", type=argparse.FileType("w"), default=stdout,
            help="Where to write output (default stdout)"
        )
        # get arguments
        args = parser.parse_args()
        input_tables = args.input_tables
        index_columns = args.index_columns
        drop_columns = args.drop_columns
        # output
        output_tsv = args.output
    # get union of tables
    result = tables_union(
        input_tables, index_columns=index_columns, drop_columns=drop_columns
    )
    # save result to output
    result.to_csv(output_tsv, sep="\t", index=bool(index_columns))
