#!/usr/bin/env python
# tx2gene.py
# Author: Joseph K Aicher
# Reads in GTF file with fields gene_name, gene_id, transcript_id to create a
# table matching transcript_id to gene_id, gene_name

import pandas as pd
import re


# compile regex for extracting gene/transcript information from gff line
gene_id = re.compile(r'gene_id "([^"]+)";')
gene_name = re.compile(r'gene_name "([^"]+)";')
transcript_id = re.compile(r'transcript_id "([^"]+)";')


def extract_line(line):
    """ Extracts (transcript_id, gene_name, gene_id) or None from GTF line
    """
    # perform search for desired keys
    search = (transcript_id.search(line),
              gene_name.search(line),
              gene_id.search(line))
    # if any of them are None, return None; we can't use this line
    if not all(search):
        return None
    # otherwise, extract the captured value
    return tuple(x.group(1) for x in search)


def extract_records(gtf_handle):
    """ Extracts set of unique transcript-gene records from GTF filehandle
    """
    records = (extract_line(x) for x in gtf_handle)  # get records
    valid_records = (x for x in records if x)  # remove None values
    return set(valid_records)  # return unique records as set


def records_to_table(records):
    """ Takes records of transcript_id, gene_name, gene_id to table
    """
    columns = ["transcript_id", "gene_name", "gene_id"]
    return pd.DataFrame.from_records(list(records), columns=columns)


def main(gtf_handle, output_tsv):
    """ Creates tx2gene table from GTF filehandle

    Creates tx2gene table from GTF file with columns:
    + transcript_id
    + gene_name
    + gene_id

    Parameters
    ----------
    gtf_handle: File like object (read)
    output_tsv: File like object (write)
    """
    # get table from gtf file
    table = records_to_table(extract_records(gtf_handle))
    # save table to output_tsv
    table.to_csv(output_tsv, sep="\t", index=False)


if __name__ == "__main__":
    import argparse
    from sys import stdin, stdout

    # create parser
    parser = argparse.ArgumentParser(
        description="Creates table of transcript ids, gene ids/names from GTF"
    )
    parser.add_argument("--gtf-path", type=argparse.FileType("r"),
                        default=stdin,
                        help="Path for GTF file to process (default stdin)")
    parser.add_argument("--output-tsv", type=argparse.FileType("w"),
                        default=stdout,
                        help="Path for output TSV file (default stdout)")

    # get arguments from command line
    args = parser.parse_args()
    
    # run main subroutine with parameters taken from command line
    try:
        main(args.gtf_path, args.output_tsv)
    except BrokenPipeError:
        pass  # no worries, assuming taking subset through pipe or similar
