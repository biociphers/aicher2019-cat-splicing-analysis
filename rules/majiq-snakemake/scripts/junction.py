#!/usr/bin/env python
"""
junction.py

Combines output from voila tsv (psi) and constitutive_quant.py to produce
quantification of LSV and constitutive junctions with columns:
    - lsv_id (str)
    - junction_id (str)
    - psi (float)
    - std_psi (float)
    - gene_id: gene id maps to...
    - gene_name (str): What is the gene name the junction associated with?
    - constitutive (bool): Is the LSV/junction constitutive?
    - nonambiguous (bool): Is the LSV/junction nonambiguous for the given group

Inputs:
    - voila_tsv: Path to voila psi tsv file
    - constitutive_tsv: Path to constitutive quantifications file
    - splicegraph: Path to splicegraph
"""


import pandas as pd
import sqlite3  # map: gene_id -> gene_name


VOILA_COLUMNS = {
    "LSV ID": "lsv_id",
    "LSV Type": "lsv_type",
    "chr": "chrom",
    "Junctions coords": "coord",
    "E(PSI) per LSV junction": "psi",
    "StDev(E(PSI)) per LSV junction": "std_psi",
}


def main(voila_psi_tsv, constitutive_tsv, splicegraph):
    """ Combines lsv/constitutive quantifications, view/annotate junctions

    Parameters
    ----------
    voila_psi_tsv: str
        Path to voila tsv file
    constitutive_tsv
        Path to constitutive quantification tsv file
    splicegraph
        Path to splicegraph database

    Returns
    -------
    pd.DataFrame
        Indexed by lsv_id, junction_id
        Columns: ["psi", "std_psi", "gene_id", "gene_name", "constitutive",
                  "nonambiguous"]
        Final dataframe combining quantifications from voila tsv (majiq psi)
        and constitutive pseudoquantifications.
    """
    # get separated out voila junction quantifications
    df_voila = separate_voila_psi_tsv(voila_psi_tsv)
    # get constitutive quantifications
    df_constitutive = pd.read_csv(
        constitutive_tsv, sep="\t", dtype="str",
        usecols=["lsv_id", "junction_id", "psi", "std_psi"]
    ).set_index(["lsv_id", "junction_id"])
    # get combined table
    df_combined = combine_lsv_constitutive_junctions(
        df_voila, df_constitutive, splicegraph
    ).sort_index()
    # annotate which lsvs are non-ambiguous
    return df_combined.join(nonambiguous_lsvs(df_combined), how="inner")


def nonambiguous_lsvs(df_junctions):
    """ Returns boolean series indexed by lsvs indicating if lsv nonambiguous

    Parameters
    ----------
    df_junctions: pd.DataFrame
        Indexed by (lsv_id, junction_id) with column gene_name

    Returns
    -------
    pd.Series
        Indexed by lsv_id, named nonambiguous, True if LSV is nonambiguous,
        False otherwise

    Notes
    -----
    1. A junction is defined as ambiguous if it is associated with multiple
       genes (distinguished by gene_name rather than gene_id).
    2. An LSV is defined as ambiguous if one of its junctions is ambiguous.
    3. An LSV is defined as nonambiguous if it is not ambiguous.
    """
    # count the number of genes associated per junction_id
    junction_gene_ct = df_junctions.groupby(
        "junction_id"
    )["gene_name"].nunique()
    # ambiguous junctions are those associated with multiple genes
    ambiguous_junctions = junction_gene_ct[junction_gene_ct > 1].index
    # ambiguous lsvs are the associated lsvs ids
    ambiguous_lsvs = df_junctions.loc(axis=0)[
        :, ambiguous_junctions
    ].index.get_level_values("lsv_id").unique()
    # create series with index of unique lsv ids, True if nonambiguous
    nonambiguous = pd.Series(
        # nonambiguous by default, for all lsvs recognized by df_junctions
        True, index=df_junctions.index.levels[0],
        # name of the series will be nonambiguous
        name="nonambiguous"
    )
    nonambiguous[ambiguous_lsvs] = False  # set ambiguous lsvs to false
    return nonambiguous


def combine_lsv_constitutive_junctions(
        lsv_junctions, constitutive_junctions,
        splicegraph_path
):
    """ Concatenates LSV/constitutive junction quantifications, annotates

    Concatenates LSV/constitutive junction quantifications and annotates with
    information about gene_id, gene_name, and constitutive status

    Parameters
    ----------
    lsv_junctions, constitutive_junctions: pd.DataFrame
        Indexed by lsv_id, junction_id
        With columns psi, std_psi
    splicegraph_path: str
        splicegraph with table gene that maps from gene_id to gene_name

    Returns
    -------
    pd.DataFrame
        Indexed by lsv_id, junction_id
        With columns psi, std_psi, gene_id, gene_name, constitutive
    """
    # combine the two
    df_combined = pd.concat(
        [lsv_junctions, constitutive_junctions],
        join="inner"
    )
    # get lsv info for df_combined
    lsv_info = get_lsv_info(df_combined.index.levels[0], splicegraph_path)
    # merge the information back into df_combined, and return
    return df_combined.join(lsv_info, how="inner")


def separate_voila_psi_tsv(voila_psi_tsv):
    """ Get junction quantifications with junction ids from voila tsv (psi)

    Parameters
    ----------
    voila_psi_tsv: str
        Path to voila tsv file

    Returns
    -------
    pd.DataFrame
        Indexed by lsv_id, junction_id
        Columns: psi, std_psi
    """
    # load relevant columns of tsv file
    df_voila = pd.read_csv(
        voila_psi_tsv, sep="\t", dtype=str,
        usecols=lambda x: x in VOILA_COLUMNS.keys()
    ).rename(columns=VOILA_COLUMNS).set_index("lsv_id")
    # separate out coordinates, psi, std_psi
    df = _separate_rows(
        df_voila, ["coord", "psi", "std_psi"], sep=";",
        explode_index="junction_ndx"
    )
    # annotate which junctions are introns -- always last junction_ndx
    max_junction = df.reset_index().groupby("lsv_id")["junction_ndx"].max()
    # get the index for where the LSVs have introns
    intron_junction = max_junction[
        # get series of max junction ndx for lsvs with introns
        df_voila["lsv_type"].str.contains("i")
    ].reset_index().set_index(
        # reset index to make dataframe, then set up multiindex to use
        ["lsv_id", "junction_ndx"]
    ).index  # get the multi-index of introns
    # mark these as being introns
    df["is_intron"] = False  # first assume that everything is an intron first
    df.loc[intron_junction, "is_intron"] = True
    # create the junction_id
    junction_coord = get_coordinates(df["coord"])
    df["junction_id"] = junction_id(
        df["chrom"], junction_coord["start"], junction_coord["end"],
        is_intron=df["is_intron"]
    )
    # update index and select resulting columns
    df = df.set_index(
        # add junction_id to index
        "junction_id", append=True
    ).reset_index(
        # remove junction_ndx, no longer necessary
        "junction_ndx", drop=True
    )[
        # keep psi, std_psi
        ["psi", "std_psi"]
    ].sort_index()
    # return resulting dataframe
    return df


def get_coordinates(coord_interval_str):
    """ Extracts columns start, end for series of interval strings

    coord_interval_str: pd.Series(str)
        Series of strings of the form {start:int}-{end:int}

    Returns
    -------
    pd.DataFrame
        indexed identically as coord_interval_str
        columns: ["start", "end"]

    Notes
    -----
    Raises ValueError if coordinate regex does not match
    """
    COORDINATE_REGEX = r"^(?P<start>\d+)-(?P<end>\d+)$"
    # extract dataframe with columns
    df_coord = coord_interval_str.str.extract(COORDINATE_REGEX)
    # check that the input was appropriate
    if df_coord.isna().any(axis=None):  # any failed to extract?
        raise ValueError(
            "get_coordinatees was given intervals with unexpected format."
        )
    # otherwise, reteurn df_coord
    return df_coord


def get_lsv_info(lsvs, splicegraph_path):
    """ Given list of LSVs, return dataframe indexed by unique LSVs with info

    Given list of LSVs, return dataframe indexed by unique LSVs with
    information, specifically the columns:
    + gene_id
    + gene_name
    + constitutive

    Parameters
    ----------
    lsvs: list-like of str
        LSVs to evaluate
    splicegraph_path: str
        splicegraph with table gene that maps from gene_id to gene_name

    Returns
    -------
    pd.DataFrame
        Indexed by unique lsvs
        Columns: [gene_id, gene_name, constitutive]
    """
    # pattern of lsvs that we are using
    LSV_REGEX = "^(?P<gene_id>gene:[^:]+):(?P<constitutive>[cst]):"
    # get set of unique lsvs
    unique_lsvs = pd.Series(pd.unique(lsvs), name="lsv_id")
    unique_lsvs.index = unique_lsvs
    # extract gene_id, lsv_type using LSV_REGEX
    lsv_table = unique_lsvs.str.extract(LSV_REGEX)
    if lsv_table.isna().any(axis=None):  # any failed to extract?
        raise ValueError(
            "get_lsv_info was given LSVs with unexpected format."
        )
    # update constitutive
    lsv_table["constitutive"] = lsv_table["constitutive"] == "c"
    # get map from gene_id to gene_name
    with sqlite3.connect(splicegraph_path) as db_splicegraph:
        df_gene = pd.read_sql_query(
            "select id,name from gene", db_splicegraph
        ).set_index("id").rename(columns={"name": "gene_name"})
    lsv_table = lsv_table.join(df_gene, on="gene_id", how="inner")
    # return the table
    return lsv_table  # columns ["gene_id", "gene_name", "constitutive"]


def junction_id(chrom, junction_start, junction_end, is_intron=False):
    """ Return junction id given coordinates

    Return junction id given coordinates. We define junction id here as
    "{chrom}:{i,j}:{junction_start}-{junction_end}", where i or j is set
    depending on whether the id is for a junction or an intron

    Parameters
    ----------
    chrom: str or pd.Series(str)
        Chromosome name for coordinates
    junction_start, junction_end: int or str or pd.Series
        Start/end of junction
    is_intron: bool or pd.Series(bool)
        specifies whether junction or intron

    Returns
    -------
    str or pd.Series(str)
        junction ids
    """
    # handle introns
    if isinstance(is_intron, pd.Series):
        intron_str = is_intron.apply(lambda x: "i" if x else "j")
    else:
        intron_str = "i" if is_intron else "j"
    # make sure junction_start, junction_end are strings
    if isinstance(junction_start, pd.Series):
        junction_start = junction_start.astype(str)
    else:
        junction_start = str(junction_start)
    if isinstance(junction_end, pd.Series):
        junction_end = junction_end.astype(str)
    else:
        junction_end = str(junction_end)
    # put everything together
    return chrom + ":" + intron_str + ":" + junction_start + "-" + junction_end


def constitutive_lsv_id(gene_id, junction_start, junction_end):
    """ Return mock LSV id for constitutive junction

    Return mock LSV id for constitutive junction. Recall that LSV ids are of
    the form {gene_id}:{s,t}:{exon_start}-{exon_end}. We replace {s,t} with c
    (for constitutive), and because both start/target exons are single, we use
    the junction coordinates.

    Parameters
    ----------
    gene_id: str or pd.Series(str)
    junction_start: int or str or pd.Series
    junction_end: int or str or pd.Series

    Returns
    -------
    str: LSV ID for the constitutive junction(s)

    Notes
    -----
    pandas series implementation present as well.
    """
    # make sure junction_start, junction_end are strings
    if isinstance(junction_start, pd.Series):
        junction_start = junction_start.astype(str)
    else:
        junction_start = str(junction_start)
    if isinstance(junction_end, pd.Series):
        junction_end = junction_end.astype(str)
    else:
        junction_end = str(junction_end)
    # put them together
    return gene_id + ":c:" + junction_start + "-" + junction_end


def _separate_rows(
        df, split_columns, sep=None,
        explode_index="explode_index", default_index="default_index",
        sort_index=True
):
    """ Given delimited values in `df[split_columns]`, separates values to rows

    Given delimited values in `df[split_columns]`, separates values to rows.
    This emulates the behavior of `tidyr::separate_rows` in R tidyverse.

    Parameters
    ----------
    df: pd.DataFrame
        dataframe with columns in `split_columns`
    split_columns: label or list of labels
        label or labels of columns in df that are delimited by sep and will be
        split into corresponding rows
    sep: Optional[str]
        string or regular expression to split on. If not specified, split using
        pandas default (e.g. split on whitespace)
    explode_index: Optional[str]
        Splitting rows adds level to index corresponding to index in split.
        This is the name that will correspond to that level.
    default_index: Optional[str]
        If any of the index labels of df are None, they must be set, this helps
        determine the name they will take in the result.
    sort_index: bool
        Determines whether index will be sorted so that split rows are
        consecutive

    Returns
    -------
    pd.DataFrame
        dataframe with split rows
    """
    # if no split_columns, then return df as is
    if not split_columns:
        return df
    # make sure split_columns is a list
    if isinstance(split_columns, str):
        split_columns = [split_columns]
    # make sure all index levels are named
    if None in df.index.names:
        df = df.copy()  # make copy of the dataframe
        df.index = df.index.rename([
            f"{default_index}_{ndx}" if old_index is None else old_index
            for ndx, old_index in enumerate(df.index.names)
        ])
    # split columns by separator
    split_series = [df[x].str.split(sep) for x in split_columns]
    # split length for these series?
    split_len = split_series[0].str.len()  # get the first column
    for ss in split_series[1:]:  # make sure the remaining columns agree
        if (split_len != ss.str.len()).any():
            raise ValueError("Target columns do not all agree on length")
    # get the maximum number of elements in a split list
    max_len = split_len.max()
    # get the unsorted exploded/separated dataframe
    explode_df = pd.concat(
        # get a dataframe with the separated columns
        (
            pd.concat(
                # concatenate separated columns
                (ss[split_len > ndx].str.get(ndx) for ss in split_series),
                axis=1, join="inner"
            )
            for ndx in range(max_len)
        ), join="inner",
        # add index value with index of split
        keys=range(max_len), names=[explode_index]
    ).join(
        # add back the non-split columns
        df.drop(columns=split_columns), how="inner"
    ).reorder_levels(
        # move the original columns back in the original order
        list(range(1, len(df.index.names) + 1)) + [0]
    )
    # do we need to sort the index?
    if sort_index:
        explode_df = explode_df.sort_index()
    # return the result
    return explode_df


if __name__ == "__main__":
    try:  # get all parameters (input, output) from snakemake
        # inputs
        voila_tsv = snakemake.input.voila_tsv
        constitutive_tsv = snakemake.input.constitutive_tsv
        splicegraph = snakemake.input.splicegraph
        # outputs
        output_tsv = snakemake.output[0]
    except NameError:  # we aren't running snakemake, try command-line
        # set up argument parser
        import argparse
        from sys import stdout
        parser = argparse.ArgumentParser(
            description="Combines quantifications from voila psi tsv (LSVs"
            " with variation for multiple junctions) and constitutive"
            " junctions, using splicegraph to annotate with useful information"
        )
        parser.add_argument(
            "voila_tsv", type=str,
            help="Path to voila tsv table for psi group quantifications"
        )
        parser.add_argument(
            "constitutive_tsv", type=str,
            help="Path to constitutive quantifications tsv file"
        )
        parser.add_argument(
            "splicegraph", type=str,
            help="Path to splicegraph"
        )
        parser.add_argument(
            "--output", type=argparse.FileType(mode="w"), default=stdout,
            help="Where to write resulting tab-separated table (default:"
            " stdout)"
        )
        # get args out
        args = parser.parse_args()
        # use same variables as snakemake
        # inputs
        voila_tsv = args.voila_tsv
        constitutive_tsv = args.constitutive_tsv
        splicegraph = args.splicegraph
        # outputs
        output_tsv = args.output
    # get the combined table
    df_combined = main(voila_tsv, constitutive_tsv, splicegraph)
    # save to output
    df_combined.to_csv(output_tsv, sep="\t", index=True)
