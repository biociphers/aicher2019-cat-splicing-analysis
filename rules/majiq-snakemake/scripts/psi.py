"""
(majiq )psi.py

Snakemake script for running `majiq psi` on a group of samples in order to
generate voila files with quantifications

Author: Joseph K Aicher

input:
  - majiq: List of "replicate" MAJIQ files that will go into the quantification
output: (at least one must be specified)
  - voila: Path for output voila file that can be used for visualization.
    Required to end with `.psi.voila` in order to infer quantification group
    name
  - tsv: Path for output tsv file (less information than from voila tsv).
    Required to end with `.psi.tsv`
params (optional):
  - tmp_prefix: if specified, prefix of temporary directory to use for
    intermediate output (otherwise use system default)
  - min_experiments: threshold for group filters (number of experiments where
    filters pass to pass LSV/junction)
  - minreads: minimum number of reads combining all positions in an event to
    be considered
  - minpos: minimum number of start positions with at least 1 read for an
    event to be considered
"""

from pathlib import Path  # nice path manipulations
from tempfile import TemporaryDirectory  # temporary directories
from snakemake.shell import shell  # shell submission via snakemake


# define extension for output VOILA file, output tsv file (if requested)
VOILA_EXTENSION = ".psi.voila"
TSV_EXTENSION = ".psi.tsv"

DEFAULT_PARAMS = {
    "min_experiments": 0.5,
    "minreads": 10,
    "minpos": 3,
    "tmp_prefix": None
}


def infer_quant_group(output_file, extension):
    try:
        assert output_file.endswith(extension), (
            f"Specified output {output_file} must end with {extension}"
        )
        return Path(output_file).name[:-len(extension)]
    except AttributeError:  # None does not have attribute endswith
        return None


def get_flags(
        min_experiments=0.5,
        minreads=10,
        minpos=3,
        output_voila=None,
        output_tsv=None,
        # pass-through
        tmp_prefix=None,
        **extra_params
):
    """ Get command-line flags from parameters passed in
    """
    flags = ""
    if output_tsv is None and output_voila is None:
        raise ValueError(
            "At least one of output.voila or output.tsv must be specified"
        )
    elif output_tsv and output_voila:
        flags += " --output-type all"
    elif output_tsv:  # not output_voila
        flags += " --output-type tsv"
    else:  # output_voila and not output_tsv
        flags += " --output-type voila"
    flags += f" --min-experiments {min_experiments}"
    flags += f" --minreads {minreads}"
    flags += f" --minpos {minpos}"
    return flags


# get log
log = snakemake.log_fmt_shell(stdout=True, stderr=True, append=True)

# get input
majiq_input = snakemake.input.majiq
assert len(majiq_input) > 0, "There must be at least one input MAJIQ file"

# get output
output_voila = snakemake.output.get("voila", None)
output_tsv = snakemake.output.get("tsv", None)

# get parameters
params = dict(DEFAULT_PARAMS)
params.update(dict(snakemake.params))
tmp_prefix = params["tmp_prefix"]
cmd_flags = get_flags(
    output_voila=output_voila, output_tsv=output_tsv, **params
)

# extract quantification group names
voila_name = infer_quant_group(output_voila, VOILA_EXTENSION)
tsv_name = infer_quant_group(output_tsv, TSV_EXTENSION)
# either one is None or they match
assert (bool(voila_name) != bool(tsv_name)) or (voila_name == tsv_name), (
    "Inferred quantification group name from"
    f" output voila file {output_voila} ({voila_name}) does not match name"
    f" from output tsv file {output_tsv} ({tsv_name})"
)
quant_group = voila_name
if quant_group is None:
    quant_group = tsv_name


# create temporary directory to perform majiq psi in
if tmp_prefix:
    # make sure the prefix for temporary directory exists
    Path(tmp_prefix).mkdir(parents=True, exist_ok=True)
    # need to handle relative paths appropriately
    tmp_prefix = f"{Path(tmp_prefix).resolve()}/"
with TemporaryDirectory(prefix=tmp_prefix) as temp_dir:
    # run the command, producing output
    shell(
        "majiq psi"  # run majiq psi
        " --nproc {snakemake.threads}"  # with these many threads
        " --output {temp_dir:q}"  # output to temporary directory
        " {majiq_input:q}"  # with provided input majiq files
        " --name {quant_group:q}"  # required experiment name to define output
        " {cmd_flags}"  # parameters, output files expected
        # redirect stdout/stderr to log
        " {log}"  # save stdout and stderr to log file
    )
    # where do we expect outputs?
    temp_voila = str(
        Path(temp_dir).joinpath(
            f"{quant_group}{VOILA_EXTENSION}"
        ).resolve()
    )
    temp_tsv = str(
        Path(temp_dir).joinpath(
            f"{quant_group}{TSV_EXTENSION}"
        ).resolve()
    )
    # move these files to desired location/name
    if output_tsv:
        shell(
            'echo "moving {temp_tsv} to {output_tsv}.." {log};'
            "mv {temp_tsv:q} {output_tsv:q};"
            'echo "moved {temp_tsv} to {output_tsv}." {log}'
        )
    if output_voila:
        shell(
            'echo "moving {temp_voila} to {output_voila}.." {log};'
            "mv {temp_voila:q} {output_voila:q};"
            'echo "moved {temp_voila} to {output_voila}." {log}'
        )
