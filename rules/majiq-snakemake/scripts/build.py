"""
(majiq )build.py

Snakemake script for running `majiq build`

Author: Joseph K Aicher

input: (samples must be unique)
  - bam: samples that are being processed ({...}/{sample}.bam)
  - sj: preprocessed junction files being processed ({...}/{sample}.sj)
  - annotation: the GFF3 file with annotations
output: (if specified)
  - sj: list of junction files to get back from majiq build
    ({...}/{sample}.sj). Samples must be subset from input.bams
  - majiq: list of majiq files to get back from majiq build
    ({...}/{sample}.majiq). Samples must be subset from either bams/sjs
  - splicegraph: single splicegraph file to save
  - conf: configuration used
  - constitutive: single output TSV with constitutive junctions (sets the
    `--dump-constitutive` flag)
params (required):
  - max_readlen: longest read length among input files -- used for allocating
    memory appropriately in majiq build
params (optional):
  - tmp_prefix: if specified, prefix of temporary directory to use for
    intermediate output (otherwise use system default)
  - build_groups: dictionary from sample name to group for building. If not
    specified, uses each sample as its own group
  - juncs_only: if True, only get the junction files
  - min_experiments: group filters threshold
  - disable_ir: if True, disables intron retention detection
  - disable_denovo: if True, disables denovo detection
  - min_intronic_cov: minimum number of reads on average in intronic sites
    for intron retention
  - min_denovo: minimum number of reads threshold to consider a denovo
    junction as real
  - minreads: minimum number of reads threshold combining all positions in an
    LSV to consider that the LSV exists in the data
  - minpos: minimum number of start positions with at least one read to
    consider an LSV existing in the data
  - k: Number of positions to sample per iteration
  - m: Number of bootstrapping samples
  - irnbins: number of bins (fraction of bins?) with some coverage that an
    intron needs to pass as real
  - annotated_ir_always: boolean flag -- when set, all annotated ir will be
    accepted even if they do not pass the filters
  - simplify_denovo: Minimum number of reads threshold combining all
    positions of an denovo junction to consider if it will be simplified,
    even knowing it is real. Simplified junctions are discarded from any lsv.
    [Default: 0]
  - simplify_annotated: Minimum number of reads threshold combining all
    positions of an annotated junction to consider if it will be simplified,
    even knowing it is real. Simplified junctions are discarded from any lsv.
    [Default: 0]
  - simplify_ir: Minimum number of reads threshold combining all positions of
    an ir to consider if it will be simplified, even knowing it is real.
    Simplified junctions are discarded from any lsv. [Default: 0]
  - simplify: Minimum fraction of the usage of any junction in a LSV to
    consider that junction is real. [Default: -1]
  - genome_name: name of genome being used (default unspecified)
  - strandness: value for strandness in majiq build (default None). Either str
    (global setting) or Dict[sample->strandness]. If Dict, use unstranded as
    default and correct +/- to forward and reverse
"""

from pathlib import Path
from tempfile import TemporaryDirectory  # temporary directories
from snakemake.shell import shell  # shell submission via snakemake
from snakemake.io import Params as SmkParams


# constants
BAM_EXTENSION = ".bam"  # required bam extension
JUNCTION_EXTENSION = ".sj"  # expected extension of input/output junction file
MAJIQ_EXTENSION = ".majiq"  # expected extension of output MAJIQ file
STRANDNESS_SHORTHAND = {  # acceptable shorthand for strandness
    "+": "forward",
    "-": "reverse",
    ".": "none",
}

# default parameters
DEFAULT_PARAMS = {
    "min_experiments": 0.5,
    "disable_ir": False,
    "disable_denovo": False,
    "min_intronic_cov": 0.01,
    "min_denovo": 5,
    "minreads": 3,
    "minpos": 2,
    "k": 50,
    "m": 30,
    "irnbins": 0.5,
    "annotated_ir_always": False,
    "simplify_denovo": 0,
    "simplify_annotated": 0,
    "simplify_ir": 0,
    "simplify": -1,
    "juncs_only": False,
    "tmp_prefix": None,
    "genome_name": "unspecified",
    "strandness": "None",
    "params_dict": dict(),
    # does not include max_readlen (required) or build_groups (default is
    # input-dependent)
}


# functions
def get_dirs_and_samples(files, req_extension):
    """ Gets lists of directories and samples from list of files with extension

    Processes list of files which are required to have an extension
    ("{dir}/{sample}{req_extension}") to obtain the directories and samples
    in the original list of files

    Parameters
    ----------
    files: List[str]
        List of files from which unique directories and samples are extracted
    req_extension: str
        Required extension for all input files

    Returns
    -------
    sample_dirs, samples: Tuple[Set[str], Set[str]]
        Sets for directories and samples

    Raises
    ------
    ValueError if a file doesn't match the required extension
    """
    # consider set of samples and directories
    sample_dirs = set()
    samples = set()
    # for each file, extract directory and sample
    for f in files:
        f_dir = Path(f).parent.resolve()  # get absolute path to its directory
        f_name = Path(f).name  # remove the parent path
        # check that it has required extension
        if not f_name.endswith(req_extension):
            raise ValueError(
                f"Input {f} does not have required extension {req_extension}"
            )
        # extract the sample name
        f_sample = f_name[:-len(req_extension)]
        # add to set of directories and samples
        sample_dirs.add(f_dir)
        samples.add(f_sample)
    return sample_dirs, samples


def assert_nonambiguous_samples(sj_samples, bam_samples, sjdirs, bamdirs):
    """ Raises error if sj_samples, bam_samples are found in multiple places

    sj_samples only raises if in multiple sjdirs -- ignores collisions in
    bamdirs. bam_samples raises if found in either sjdirs or bamdirs multiple
    times

    Parameters
    ----------
    sj_samples: Collection[str]
        Sample names for sj files
    bam_samples: Collection[str]
        Sample names for bam files
    sjdirs: Collection[str]
        Directories for sj files
    bamdirs: Collection[str]
        Directories for bam files

    Returns
    -------
    None
    """
    # make sure we don't have any ambiguity about samples
    repeated_samples = set.intersection(bam_samples, sj_samples)
    if repeated_samples:
        raise ValueError(
            f"Samples {repeated_samples} were input in both input.bam and input.sj"
        )
    for sample in sj_samples:
        paths = set()
        for sjdir in sjdirs:
            # check if sample is in this sjdir
            potential_path = sjdir.joinpath(f"{sample}{JUNCTION_EXTENSION}")
            if potential_path.exists():
                # add it to set of paths for this sample
                paths.add(potential_path)
        if len(paths) > 1:
            paths = ", ".join(str(x) for x in paths)
            raise ValueError(
                "Because input files are specified in MAJIQ as directories and"
                " samples, there is a potential for ambiguity in inputs."
                " Sample {sample} was ambiguous because it is found in {paths}."
            )
    for sample in bam_samples:
        paths = set()
        for sjdir in sjdirs:
            # check if sample is in this sjdir
            potential_path = sjdir.joinpath(f"{sample}{JUNCTION_EXTENSION}")
            if potential_path.exists():
                # add it to set of paths for this sample
                paths.add(potential_path)
        for bamdir in bamdirs:
            # check if sample is in this bamdir
            potential_path = bamdir.joinpath(f"{sample}{BAM_EXTENSION}")
            if potential_path.exists():
                # add it to set of paths for this sample
                paths.add(potential_path)
        if len(paths) > 1:
            paths = ", ".join(str(x) for x in paths)
            raise ValueError(
                "Because input files are specified in MAJIQ as directories and"
                " samples, there is a potential for ambiguity in inputs."
                " Sample {sample} was ambiguous because it is found in {paths}."
            )
    return


def correct_strandness(strandness):
    """ Correct shorthand for strandness, raise error if not valid

    strandness: str
        Should be none, forward, or reverse. Case-insensitive. Shorthand
        "+,-,." will be accepted by Snakemake wrapper and transformed
        appropriately. Raise error otherwise.
    """
    # acceptable shorthand
    try:
        strandness = STRANDNESS_SHORTHAND[strandness]
    except KeyError:
        pass
    # make lower-case
    strandness = strandness.lower()
    if strandness not in ("none", "forward", "reverse"):
        raise ValueError(
            f"Strandness {strandness} invalid (should be none|forward|reverse)"
        )
    return strandness


def get_flags(
        min_experiments=0.5,
        disable_ir=False,
        disable_denovo=False,
        min_intronic_cov=0.01,
        min_denovo=5,
        minreads=3,
        minpos=2,
        k=50,
        m=30,
        irnbins=0.5,
        annotated_ir_always=False,
        simplify_denovo=0,
        simplify_annotated=0,
        simplify_ir=0,
        simplify=-1,
        juncs_only=False,
        output_constitutive=None,
        # unused parameters to capture from extra_params
        tmp_prefix=None,
        genome_name="unspecified",
        strandness="None",
        build_groups=None,
        max_readlen=None,
        params_dict=dict(),
        **extra_params
):
    """ Get command-line flags from parameters passed in

    Parameters
    ----------
    min_experiments: float
        Lower threshold for group filters. min_experiments set the minimum
        number of experiments where the different filters check in order to
        pass an LSV or junction. A value < 1 means the value is the fraction of
        experiments in the group. A value >= 1 means the value is the actual
        number of experiments. If the number is set to a larger number than the
        size of the group, we use the size of the group instead.
    disable_ir: bool
        Disables intron retention detection
    disable_denovo: bool
        Disables denovo detection of junction, splicesites and exons. This will
        speedup the execution but reduce the number of LSVs detected.
    min_intronic_cov: float
        Minimum number of reads on average in intronic sites, only for intron
        retention.
    min_denovo: int
        Minimum number of reads threshold combining all positions in a LSV to
        consider that denovo junction is "real".
    minreads: int
        Minimum number of reads threshold combining all positions in a LSV to
        consider that the LSV "exist in the data".
    minpos: int
        Minimum number of start positions with at least 1 read in a LSV to
        consider that the LSV "exist in the data".
    k: int
        Number of positions to sample per iteration.
    m: int
        Number of bootstrapping samples.
    irnbins: float
        This values defines the number of bins with some coverage that an
        intron needs to pass to be accepted as real.
    annotated_ir_always: bool
        When this flag is set all the annotated ir will be accepted even if
        they do not pass the filters.
    simplify_denovo: bool
        Minimum number of reads threshold combining all positions of an denovo
        junction to
        consider if it will be simplified, even knowing it is real.
        Simplified junctions are discarded from any lsv.
    simplify_annotated: bool
        Minimum number of reads threshold combining all positions of an
        annotated junction to consider if it will be simplified, even knowing
        it is real.  Simplified junctions are discarded from any lsv.
    simplify_ir: bool
        Minimum number of reads threshold combining all positions of an ir to
        consider if it will be simplified, even knowing it is real.  Simplified
        junctions are discarded from any lsv.
    simplify: float
        Minimum fraction of the usage of any junction in a LSV to consider that
        junction is real.
    juncs_only: bool
        Only produce sj files from bam files
    output_constitutive: Optional[str]
        If not None, path for dumped constitutive junctions from majiq build
        which requires a command-line flag to be specified

    tmp_prefix: Optional[str]
        Passed through
    genome_name: str
        Passed through
    strandness: str
        Passed through
    build_groups: Optional[Dict[str, str]]
        Passed through
    max_readlen: Optional[int]
        Passed through
    params_dict: Dict[str, Any]
        Passed through
    **extra_params: Any
        Extra unexpected parameters which will be described in a warning, if
        any

    Returns
    -------
    str
        command-line flags for majiq build
    """
    flags = ""
    if output_constitutive:
        flags += " --dump-constitutive"
    if juncs_only:
        flags += " --junc-files-only"
    if disable_ir:
        flags += " --disable-ir"
    if disable_denovo:
        flags += " --disable-denovo"
    if annotated_ir_always:
        flags += " --annotated_ir_always"
    flags += f" --min-experiments {min_experiments}"
    flags += f" --min-intronic-cov {min_intronic_cov}"
    flags += f" --min-denovo {min_denovo}"
    flags += f" --minreads {minreads}"
    flags += f" --minpos {minpos}"
    flags += f" --k {k}"
    flags += f" --m {m}"
    flags += f" --irnbins {irnbins}"
    flags += f" --simplify-denovo {simplify_denovo}"
    flags += f" --simplify-annotated {simplify_annotated}"
    flags += f" --simplify-ir {simplify_ir}"
    flags += f" --simplify {simplify}"
    if extra_params:
        print(f"Warning: Extra params passed into Snakemake: {extra_params}")
    return flags


# get log
log = snakemake.log_fmt_shell(stdout=True, stderr=True, append=True)

# get inputs and normalize (make sure lists are lists)
annotation = snakemake.input.annotation
input_bam = snakemake.input.get("bam", list())  # list of bam files
if not isinstance(input_bam, list):
    input_bam = [input_bam]
input_sj = snakemake.input.get("sj", list())  # list of sj files
if not isinstance(input_sj, list):
    input_sj = [input_sj]

# get outputs and normalize (make sure lists are lists)
output_splicegraph = snakemake.output.get("splicegraph", None)
output_conf = snakemake.output.get("conf", None)
output_constitutive = snakemake.output.get("constitutive", None)
output_sj = snakemake.output.get("sj", list())  # list of sj files
if not isinstance(output_sj, list):
    output_sj = [output_sj]
output_majiq = snakemake.output.get("majiq", list())  # list of majiq files
if not isinstance(output_majiq, list):
    output_majiq = [output_majiq]

# get snakemake parameters
params = dict(DEFAULT_PARAMS)  # start with defaults
params.update(dict(snakemake.params))  # update with params from snakemake
params.update(params.get("params_dict", dict()))  # update from dictionary
# get parameters that don't go into majiq build but instead deal with
# config/path we are doing things in
tmp_prefix = params["tmp_prefix"]
genome_name = params["genome_name"]
strandness = params["strandness"]
max_readlen = params.get("max_readlen", None)  # required parameter
assert max_readlen, "max_readlen is a required parameter"
# get optional args for passing into majiq build
if params["juncs_only"]:
    # if juncs_only, check that we aren't requesting majiq files as output
    assert len(output_majiq) == 0, (
        "params.juncs_only implies that output.majiq should be empty"
    )
optional_args = get_flags(output_constitutive=output_constitutive, **params)


# get {bam,sj}dirs and sample names
bamdirs, bam_samples = get_dirs_and_samples(input_bam, BAM_EXTENSION)
sjdirs, sj_samples = get_dirs_and_samples(input_sj, JUNCTION_EXTENSION)
# assert nonambiguous samples
assert_nonambiguous_samples(sj_samples, bam_samples, sjdirs, bamdirs)
# since we are happy with input samples, we can combine our list
samples = set.union(bam_samples, sj_samples)

# get the strandness that we want
if isinstance(strandness, str):
    strandness = correct_strandness(strandness)
    strandness_override = dict()
elif isinstance(strandness, dict):
    # if sample dictionary of strandness, default is unstranded, override
    # per valid sample
    strandness = correct_strandness("none")
    strandness_override = {
        sample: correct_strandness(value)
        for sample, value in strandness.items()
        if sample in samples
    }
else:
    raise ValueError(
        f"Parameter strandness ({strandness}) was passed with wrong type."
        f" Expected str or Dict[str, str], got {type(strandness)}..."
    )

# get the build groups we are using -- if not present use individual samples
build_groups = params.get("build_groups", {s: s for s in samples})
# filter to only have the input samples
build_groups = {s: g for s, g in build_groups.items() if s in samples}
# transpose to get samples per group
group_samples = dict()
for s, g in build_groups.items():
    try:
        # add to the list (as string)
        group_samples[g] += f",{s}"
    except KeyError:  # haven't added this group yet, start the string list
        group_samples[g] = f"{s}"

# output junction files need to be for samples for which we are using bams
_, output_sj_samples = get_dirs_and_samples(output_sj, JUNCTION_EXTENSION)
if not output_sj_samples.issubset(bam_samples):
    no_bam_samples = ", ".join(output_sj_samples.difference(bam_samples))
    raise ValueError(
        "Input BAM file was not provided for requested junction files for"
        f" samples {no_bam_samples}"
    )
# output majiq files need to be for samples we are processing
_, majiq_samples = get_dirs_and_samples(output_majiq, MAJIQ_EXTENSION)
if not majiq_samples.issubset(samples):
    invalid_samples = ", ".join(majiq_samples.difference(samples))
    raise ValueError(
        "Requested MAJIQ files for samples that were not input:"
        f" {invalid_samples}"
    )



# start setting up files to do things
if tmp_prefix:
    # make sure the prefix for temporary directory exists
    Path(tmp_prefix).mkdir(parents=True, exist_ok=True)
    # need to handle relative paths appropriately
    tmp_prefix = f"{Path(tmp_prefix).resolve()}/"
with TemporaryDirectory(prefix=tmp_prefix) as temp_dir:
    # set up configuration for majiq build
    conf = Path(temp_dir).joinpath("build.ini")
    with conf.open("w") as handle:
        print("[info]", file=handle)
        # print the read length
        print(f"readlen={max_readlen}", file=handle)
        # print the sj directories
        if sjdirs:
            sjdirs = ",".join(str(x) for x in sjdirs)
            print(f"sjdirs={sjdirs}", file=handle)
        # print the bam directories
        if bamdirs:
            bamdirs = ",".join(str(x) for x in bamdirs)
            print(f"bamdirs={bamdirs}", file=handle)
        # print the genome name
        print(f"genome={genome_name}", file=handle)
        # print strandness value (TODO: handle strandness more appropriately)
        print(f"strandness={strandness}", file=handle)
        # line break before experiments
        print("", file=handle)
        # experiments section with one sample
        print("[experiments]", file=handle)
        for g, s in group_samples.items():
            print(f"{g}={s}", file=handle)
        # handle any overriden strandness
        if strandness_override:
            print("", file=handle)
            print("[optional]", file=handle)
            for sample, sample_strand in strandness_override.items():
                print(f"{sample}=strandness:{sample_strand},", file=handle)
    # get fully resolved path to newly created configuration file
    conf = str(conf.resolve())
    if output_conf:
        # save the configuration to final output location
        shell("cp {conf:q} {output_conf:q}")

    # done setting up majiq build configuration, run majiq build on our file
    shell(
        "majiq build"  # run majiq build
        " --incremental"  # we are using incremental
        " --nproc {snakemake.threads}"  # with these many threads
        " --output {temp_dir:q}"  # output result to this directory
        " --conf {conf:q}"  # input configuration (specifying bam file)
        " {annotation:q}"  # input annotation (gff3 file)
        "{optional_args}"  # optional arguments
        " {log}"  # redirect stdout/stderr to file
    )

    # get remaining requested output files
    if output_splicegraph:
        temp_splicegraph = str(
            Path(temp_dir).joinpath("splicegraph.sql").resolve()
        )
        shell(
            'echo "moving {temp_splicegraph} to {output_splicegraph}..." {log};'
            "mv {temp_splicegraph:q} {output_splicegraph:q};"
            'echo "moved {temp_splicegraph} to {output_splicegraph}." {log}'
        )
    if output_constitutive:
        temp_constitutive = str(
            Path(temp_dir).joinpath("constitutive_junctions.tsv").resolve()
        )
        shell(
            'echo "moving {temp_constitutive} to {output_constitutive}..." {log};'
            "mv {temp_constitutive:q} {output_constitutive:q};"
            'echo "moved {temp_constitutive} to {output_constitutive}." {log}'
        )
    for output_file in output_sj + output_majiq:
        temp_file = str(
            Path(temp_dir).joinpath(Path(output_file).name).resolve()
        )
        shell(
            'echo "moving {temp_file} to {output_file}..." {log};'
            "cp {temp_file:q} {output_file:q};"
            'echo "moved {temp_file} to {output_file}." {log}'
        )
shell('echo "majiq build completed successfully!" {log}')
