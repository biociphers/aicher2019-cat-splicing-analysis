"""
(voila )tsv.py

Snakemake script for running `voila tsv` on a voila file and splicegraph to
generate tsv output with quantifications

Author: Joseph K Aicher

input:
  - splicegraph: splicegraph from majiq build
  - voila: voila file being used to generate TSV
output: tsv file
params:
  - tmp_prefix: if specified, prefix of temporary directory to use for
    intermediate output (otherwise use system default)
"""

from pathlib import Path  # nice path manipulations
from tempfile import TemporaryDirectory  # temporary directories
from snakemake.shell import shell  # shell submission via snakemake


# get log
log = snakemake.log_fmt_shell(stdout=True, stderr=True, append=True)

# get input
splicegraph = snakemake.input.get("splicegraph", None)
voila = snakemake.input.get("voila", None)
assert (splicegraph is not None) and (voila is not None), (
    "splicegraph and voila are required inputs"
)

# get output
output_tsv = snakemake.output[0]


# create temporary directory to perform voila tsv in
tmp_prefix = snakemake.params.get("tmp_prefix", None)
if tmp_prefix:
    # make sure the prefix for temporary directory exists
    Path(tmp_prefix).mkdir(parents=True, exist_ok=True)
    # need to handle relative paths appropriately
    tmp_prefix = f"{Path(tmp_prefix).resolve()}/"
with TemporaryDirectory(prefix=tmp_prefix) as temp_dir:
    temp_tsv = str(Path(temp_dir).joinpath("tmp.tsv"))  # temporary tsv
    # run the command, producing output
    shell(
        "voila tsv"  # run voila tsv
        " --nproc {snakemake.threads}"  # with these many threads
        " --file-name {temp_tsv:q}"  # to this temporary output file
        " --show-all"  # showing all events, no filters
        " {splicegraph:q}"  # using input splicegraph
        " {voila:q}"  # and input voila from some quantification step
        " {log}"  # and log stdout/stderr too
    )
    # move output tsv from temporary directory to final output
    shell(
        'echo "moving {temp_tsv} to {output_tsv}.." {log};'
        "mv {temp_tsv:q} {output_tsv:q};"
        'echo "moved {temp_tsv} to {output_tsv}." {log}'
    )
