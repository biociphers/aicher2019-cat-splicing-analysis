#!/usr/bin/env python
"""
constitutive.py

Uses results from majiq build --dump-constitutive and majiq build (sj files) to
produce enumeration of "LSVs" from constitutive_build that met criteria for
quantifiability and the number/percent of samples that satisfied these criteria

input:
    - constitutive: TSV with constitutive junctions from
      `majiq build --dump-constitutive`
    - sj: List of "replicate" majiq splice junction files that will go into
      pseudo-quantification
output:
    - Path for output tsv file resulting from quantification
params:
    - min_experiments: threshold for group filters (number of experiments where
      filters pass to pass LSV/junction)
    - minreads: minimum number of reads combining all positions in an event to
      be considered
    - minpos: minimum number of start positions with at least 1 read for an
      event to be considered

Author: Joseph K Aicher
"""

# imports
import pandas as pd
import numpy as np
from junction import junction_id, constitutive_lsv_id
from sys import stderr


# constants
MIN_EXPERIMENTS = 0.5
MINREADS = 10
MINPOS = 3
# what columns do we use and how do we rename/types?
CONSTITUTIVE_COLUMNS = {
    "#GENEID": "gene_id",
    "GENEID": "gene_id",
    "CHROMOSOME": "chromosome",
    "JUNC_START": "start",
    "JUNC_END": "end",
}
CONSTITUTIVE_TYPES = {
    "#GENEID": str,
    "GENEID": str,
    "CHROMOSOME": str,
    "JUNC_START": int,
    "JUNC_END": int
}


def constitutive_sample_quantifiable(
        df_constitutive, sj_file,
        minreads=MINREADS, minpos=MINPOS, logfile=stderr
):
    """ Get gene/start/end of sample-quantifiable constitutive junctions

    Parameters
    ----------
    df_constitutive: pd.DataFrame
        dataframe with columns chromosome, start, end, gene_id
    sj_file: str
        Path to MAJIQ splice junction file
    minreads, minpos:
        Only accept junctions from this sample with counts greater than these
    logfile: fileobj
        Where status messages should be written

    Returns
    -------
    pd.DataFrame (columns: gene_id, start, end)
        table of junctions that meet criteria for the sample
    """
    if logfile:
        print(
            f"Processing '{sj_file}' for constitutive junctions passing"
            " per-sample filters...",
            file=logfile
        )
    # load the sj file, and convert to dataframe
    with np.load(sj_file) as sj_data:
        df_sj = pd.DataFrame.from_records(
            sj_data["junc_info"]
        )
    # rename the columns
    df_sj.columns = [
        "junction_id", "start", "end", "reads", "pos", "intron"
    ]
    # ignore introns
    df_sj = df_sj.loc[df_sj["intron"] == 0]
    # obtain chromosome of junction from junction_id
    df_sj["chromosome"] = df_sj["junction_id"].str.decode(
        "utf-8"
    ).str.partition(":")[0]
    # obtain the subset of junctions that match constitutive junctions, match
    # to gene ids and count reads/positions
    df_counts = pd.merge(
        df_sj[["chromosome", "start", "end", "reads", "pos"]],
        df_constitutive[["chromosome", "start", "end", "gene_id"]],
        how="inner", on=["chromosome", "start", "end"]
    ).groupby(
        ["gene_id", "start", "end"]
    )[["reads", "pos"]].sum()
    # filter on minreads and pos, and get resulting index as dataframe
    result = df_counts.loc[
        (df_counts["reads"] >= minreads)
        &
        (df_counts["pos"] >= minpos)
    ].index.to_frame()
    if logfile:
        print(
            f"Processed '{sj_file}' for constitutive junctions"
            " passing per-sample filters.",
            file=logfile
        )
    return result


def group_junctions(sample_quants, min_experiments, logfile=stderr):
    """ Obtain subset of junctions present in at least min_experiments

    Parameters
    ----------
    sample_quants: Iterator[pd.DataFrame]
        Iterator over dataframes produced by `constitutive_sample_quantifiable`
        (with columns `gene_id`, `start`, `end`)
    min_experiments: int
        The minimum number of experiments a junctions must be present in to be
        saved
    logfile: fileobj
        Where status messages should be written

    Returns
    -------
    pd.DataFrame indexed by `gene_id`, `start`, `end` for junctions that pass
    group filters with columns `num_quant` and `pct_quant` to indicate how many
    experiments passed the group filters for quantifiability
    """
    # get the number of experiments for each junction
    sample_quants = list(sample_quants)
    if logfile:
        print(
            "Concatenating loaded quantifiable samples to apply group"
            " filters...",
            file=logfile
        )
    combined_experiments = pd.concat(
        sample_quants, join="inner", ignore_index=True
    )
    if logfile:
        print("Counting junctions with enough experiments...", file=logfile)
    # get the number of experiments that met individual experiment filters for
    # each junction
    num_experiments = combined_experiments.groupby(
        ["gene_id", "start", "end"]
    ).size()
    # reduce to junctions that met group filters: values are now the number of
    # experiments that were quantifiable for the junction
    num_experiments = num_experiments[
        # apply group filter
        num_experiments >= min_experiments
    ].rename("num_quant")
    # get the percent quantified
    pct_quant = (num_experiments / len(sample_quants)).rename("pct_quant")
    # combine these two into a single dataframe and return
    return pd.concat(
        [num_experiments, pct_quant],
        axis=1, join="inner"
    )


def main(
        constitutive, sj,
        min_experiments=MIN_EXPERIMENTS, minreads=MINREADS, minpos=MINPOS,
        logfile=stderr
):
    """

    Parameters
    ----------
    constitutive: str
        Path to table of constitutive junctions from `constitutive_build.py`
    sj: List[str]
        List of paths to sj files from majiq build to have constitutive
        junctions evaluated for quantifiability
    min_experiments: float
        Postive-valued threshold for group filters (<1 ~ proportion of number
        of sj files, >=1 is the number of sj files)
    minreads: int
        Minimum number of reads combining all positions in the event to be
        considered
    minpos: int
        Minimum number of start positions with at least 1 read for an event to
        be considered
    logfile: fileobj
        Where status messages should be written

    Returns
    -------
    pd.DataFrame
        Pandas dataframe with columns `lsv_id`, `junction_id`, `num_quant`,
        `pct_quant`
    """
    # turn min_experiments into an integer value as done in majiq
    if min_experiments <= 0:
        raise ValueError(
            "min_experiments must be positive but was set to"
            f" {min_experiments}"
        )
    elif min_experiments < 1:
        # it's a proportion of the number of input files
        min_experiments = np.ceil(min_experiments * len(sj))
    # otherwise, it is the same value
    # either case, make sure it's an integer and no greater than number of sj
    min_experiments = int(min(min_experiments, len(sj)))
    # load the constitutive table
    df_constitutive = pd.read_csv(
        constitutive, sep="\t", usecols=lambda x: x in CONSTITUTIVE_COLUMNS,
        dtype=CONSTITUTIVE_TYPES
    ).rename(columns=CONSTITUTIVE_COLUMNS)
    # lazily evaluate per-experiment quantifiable junctions
    sample_quants = (
        constitutive_sample_quantifiable(
            df_constitutive, x, minreads=minreads, minpos=minpos,
            logfile=logfile
        )
        for x in sj
    )
    # obtain junctions that meet group filters (min_experiments)
    group_quant = group_junctions(
        sample_quants, min_experiments, logfile=logfile
    )
    # match to constitutive table
    full_result = df_constitutive.join(
        group_quant, on=["gene_id", "start", "end"], how="inner"
    )
    # add lsv/junction ids, psi values
    full_result = full_result.assign(
        # set lsv_id
        lsv_id=constitutive_lsv_id(
            full_result["gene_id"], full_result["start"], full_result["end"]
        ),
        # set junction_id
        junction_id=junction_id(
            full_result["chromosome"], full_result["start"],
            full_result["end"], is_intron=False
        ),
        # psi/std_psi are set by definition because there is no variation
        psi=1., std_psi=0.
    )
    # keep the specified columns...
    return full_result[
        ["lsv_id", "junction_id", "psi", "std_psi", "num_quant", "pct_quant"]
    ]


if __name__ == "__main__":
    try:  # get all parameters (input, output, params from snakemake
        # get inputs from snakemake
        constitutive = snakemake.input.constitutive
        sj = snakemake.input.sj
        # get output from snakemake
        output_tsv = snakemake.output[0]
        # get optional parameters
        min_experiments = snakemake.params.get(
            "min_experiments", MIN_EXPERIMENTS
        )
        minreads = snakemake.params.get(
            "minreads", MINREADS
        )
        minpos = snakemake.params.get(
            "minpos", MINPOS
        )
        # get logfile?
        try:
            logfile = open(snakemake.log[0], "w")
        except (NameError, KeyError, IndexError):
            logfile = stderr
    except NameError:  # we aren't running snakemake, try command-line args
        # set up argument parser
        import argparse
        from sys import stdout
        parser = argparse.ArgumentParser(
            description="Obtains list of constitutive junctions/false-LSVs"
            " from data that meet group criteria for quantifiability if they"
            " were part of a proper LSV"
        )
        parser.add_argument(
            "constitutive", type=str,
            help="Path to constitutive junctions table from"
            " `majiq build --dump-constitutive`"
        )
        parser.add_argument(
            "sj", type=str, nargs="+",
            help="The splice junction files to analyze"
        )
        parser.add_argument(
            "--output", type=argparse.FileType(mode="w"), default=stdout,
            help="Where to write tab-separated table of quantifications of"
            "constitutive junctions (default: stdout)"
        )
        parser.add_argument(
            "--min-experiments", default=MIN_EXPERIMENTS, type=float,
            help="Lower threshold for group filters. min_experiments set the"
            "minimum number of experiments where the different filters check"
            " in order to pass an lsv or junction.\n\t + <  1 the value is the"
            "fraction of the experiments in the group\n\t + >= 1 the value is"
            "the actual number of experiments. If the number is set to a"
            "greater number than the size of the group, we use the size"
            "instead.\n[Default: %(default)s]]"
        )
        parser.add_argument(
            "--minreads", default=MINREADS, type=int,
            help="Minimum number of reads combining all positions in an event"
            " to be considered. [Default: %(default)s]"
        )
        parser.add_argument(
            "--minpos", default=MINPOS, type=int,
            help="Minimum number of start positions with at least 1 read for"
            " an event to be considered. [Default: %(default)s]"
        )
        parser.add_argument(
            "--logfile", type=argparse.FileType(mode="w"), default=stderr,
            help="Path to output log file (default: stderr)"
        )
        # get args out
        args = parser.parse_args()
        # get variables as done by snakemake option
        constitutive = args.constitutive
        sj = args.sj
        output_tsv = args.output
        min_experiments = args.min_experiments
        minreads = args.minreads
        minpos = args.minpos
        logfile = args.logfile
    # run the quantification to get the resulting table
    quant = main(
        constitutive, sj,
        min_experiments=min_experiments, minreads=minreads, minpos=minpos,
        logfile=logfile
    )
    # write table to file
    if logfile:
        print("Saving result to file...", file=logfile)
    quant.to_csv(output_tsv, sep="\t", index=False)
    if logfile:
        print("Finished successfully!", file=logfile)
