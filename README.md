# README

"Mapping RNA splicing variations in clinically-accessible and non-accessible
tissues to facilitate Mendelian disease diagnosis using RNA-seq", accepted to
_Genetics in Medicine_ ([biorxiv][cats-biorxiv], link to final paper pending)

License: BSD 3-Clause (see `LICENSE.md`).

Authors:

+ Aicher, Joseph K
+ Jewell, Paul
+ Vaquero-Garcia, Jorge
+ Barash, Yoseph\*
+ Bhoj, Elizabeth J\*

This repository holds Snakemake pipelines for selecting samples for analysis
and performing analyses on aligned samples to generate tables, plots, and other
statistics as reported in the paper.

[TOC]

Note that the analysis uses [MAJIQ][majiq], which has separate
[academic][majiq-academic-license]/[commercial][majiq-commercial-license]
licenses which need to be accepted to gain access to the software and download
a required configuration file which the pipeline can use to download/set up
MAJIQ (described below).

   [cats-biorxiv]: https://doi.org/10.1101/727586 "The original preprint"
   [majiq]: https://majiq.biociphers.org/ "MAJIQ homepage"
   [majiq-academic-license]: https://majiq.biociphers.org/app_download/ "MAJIQ academic license/download"
   [majiq-commercial-license]: https://majiq.biociphers.org/commercial.php "MAJIQ commercial license/terms"


## Setup

### Resources and protected files

A configuration file (JSON or YAML format) must be set up to provide paths to
specific resources. In particular:

+ Genome-related files (`config["resources"]["genome"]`): name and paths to
  GFF3/GTF transcript annotations. For our original analysis, we used
  [Ensembl v94 human annotations](ftp://ftp.ensembl.org/pub/release-94/).
  Did not include in repository due to file size.
+ HGMD VCF (`config["resources"]["hgmd_vcf"]`): VCF file from HGMD (hg38) from
  which disease gene annotations are inferred. For our original analysis, we
  used HGMD 2018.3, but cannot include this input file due to licensing
+ Salmon transcript quantifications (`config["resources"]["quant_output"]`):
  Path to folder with subfolders named after samples, each with file `quant.sf`
  produced by Salmon
+ STAR two-pass genome alignments (`config["resources"]["bamdir"]`):
  Path to folder with indexed/sorted BAM files for each sample

Additionally, by default, the MAJIQ wrapper scripts used will output temporary
files to a subdirectory of `/tmp`. Unless the instance/node you are running the
`majiq build` step on has 250G available for this directory, you will need to
update the value `config["resources"]["majiq"]["build"]["tmp_prefix"]` to point
to where you would like the temporary files to go.

The pipeline requires a table from protected access on dbGaP to provide GTEx
sample attributes for sample selection. The path it looks for is provided/set
by `config["sample_gtex"]["sample_attributes"]`, which by default points to
`data/protected/phs000424.v7.pht002743.v7.p2.c1.GTEx_Sample_Attributes.GRU.txt`.
Please place the file there or set a different path in your input
configuration. This file can be obtained from dbGaP.

Please see `config/resources_mock.yaml` for an example of what values need to
be specified.

You can also update this file to modify additional parameters of the analysis.


### Environment

Requires:

+ `conda`
+ `R` (v3.6.0) with packages
    - `tidyverse` (v1.2.1)
    - `magrittr` (v1.5)
    - `rprojroot` (v1.3-2)
    - `egg` (v0.4.5)
+ `htslib` compiled with OpenMP. Set the environment variables
  `HTSLIB_{LIBRARY,INCLUDE}_DIR` to point to the headers/shared libraries for
  htslib.

Create the environment for running the Snakemake pipelines by running in the
base directory:

```
conda env create --file environment.yaml
```

This creates the environment `cat-paper-2019-final`, which can be activated
using `conda activate cat-paper-2019-final`. Unless otherwise specified,
instructions on how to run the analysis assume that this environment is active.


### MAJIQ

Snakemake largely takes care of setting up the remaining required environments
for the analysis pipeline. However, as previously mentioned, MAJIQ requires an
[academic][majiq-academic-license]/[commercial][majiq-commercial-license] to be
accepted before use, including in this analysis pipeline.

This analysis requires features of MAJIQ that have not made it into the general
release. Accepting the license will give you the link to the
`biociphers/majiq-paper-versions` repository. From here, download the file
`majiq-for-cats.yaml` (in branch `configurations`, git tag for release:
`config-majiq-for-cats-release`), which points to a
separate branch/commit (branch `majiq-for-cats`, git tag for release:
`source-majiq-for-cats-release`) with an updated version of MAJIQ with the
features necessary to reproduce our analysis.  Download the file
`majiq-for-cats.yaml` and place it in the folder
`rules/envs/`.

As previously mentioned, MAJIQ requires `htslib` compiled with OpenMP to ensure
Snakemake/pip can build the program from source ensure that Snakemake can build
the program from source. Once these variables are set and the
`majiq-for-cats.yaml` file is in `rules/envs/`, Snakemake will set up and
manage the required version of MAJIQ for the analysis.

For more information about how to install/configure `htslib` for MAJIQ, see the
`majiq-paper-versions` README (branch `configurations`) and/or the `htslib`
docs.


## Selecting samples for analysis

To select RNA-seq samples per tissue for analysis, run:

```
snakemake --use-conda --snakefile samples.snake --configfile {user_resources}
```

where `{user_resources}` is replaced by the configuration file created as
described in "Setup -> Resources and protected files".
This will create a folder `samples/` with metadata about selected samples.

This performs the following steps/snakemake rules:

1. `sample_gtex`: Selects reproducible random subset of
   [`data/GTExRunTable.txt`](data/GTExRunTable.txt) for analysis per tissue in
   GTEx
2. `sample_hdbr`: Selects reproducible random subset of
   [`data/HDBRFetalBrainAERunTable.sdrf.txt`](data/HDBRFetalBrainAERunTable.sdrf.txt)
   for analysis per fetal brain tissue
3. `majiq_tables`: Combines samples from `sample_gtex`, `sample_hdbr` and
   [`data/FetalHeartAERunTable.sdrf.txt`](data/FetalHeartAERunTable.sdrf.txt)
   to specify groups of samples for the majiq builder and majiq quantifiers.


## Performing analysis

Run:

```
snakemake --use-conda --snakefile analysis.snake --configfile {user_resources}
```
where `{user_resources}` is replaced by the configuration file created as
described in "Setup -> Resources and protected files".
This will create a folder `output/` with the output files from the analysis.

The first time this command is used, it will automatically set up required
conda virtual environments for analysis, including one with MAJIQ. This will
clone and compile MAJIQ. This currently requires the environment variables
`HTSLIB_LIBRARY_DIR` and `HTSLIB_INCLUDE_DIR` as previously described.

This performs the following steps from the following groups of rules
(graphical view of dependencies between rules:
[`rules/rulegraph.pdf`](rules/rulegraph.pdf); view of dependency graph for dry
run with 2 CATs, 2 non-CATs with 3 samples each:
[`rules/dag.pdf`](rules/dag.pdf)):

1. [`tx2gene.snake`](rules/tx2gene.snake): match Ensembl transcript ids to gene
   ids and names
2. [`filtering_pathogenic.snake`](rules/filtering_pathogenic.snake) (once)
    1. `filtering_pathogenic_clinvar`: extract disease-causing genes from
       `data/clinvar_gene_diseases.txt`
    2. `filtering_pathogenic_hgmd`: extract disease-causing genes from
       `config["resources"]["hgmd_vcf"]` (see setup)
    3. `filtering_pathogenic_combined`: combine lists of disease-causing genes
3. [`tissue_expression.snake`](rules/tissue_expression.snake) (over all
   samples, per tissue)
    1. `tissue_expression_calculate`: summarize expression over samples per
       tissue
4. [`splicing.snake`](rules/splicing.snake)/[`majiq-snakemake/Snakefile`](rules/majiq-snakemake/Snakefile)
   (over all samples)
    1. `majiq_build`: Determine splicegraph of annotated and denovo splicing
       events, combining all samples
    2. `majiq_psi`: Quantify local splicing events per sample
    3. `voila_tsv`: Obtain detailed information about quantifications as
       tab-separated files
    4. `majiq_constitutive`: Identify "quantifiable" constitutive junctions per
       sample
    5. `voila_junctions`: Provide tidy (long) view of quantified junctions and
       introns for local splicing variations and constitutive junctions
5. [`filtering_base.snake`](rules/filtering_base.snake) (over all comparisons
   between possible CATs/non-CATs)
    1. `filtering_base_junctions_nonCAT_consistent`: identify consistent
       junctions per non-CAT using quantifications from `voila_junctions`
    2. `filtering_base_junctions_CAT_nonrepresented_pairwise`: identify
       nonrepresented junctions per CAT and non-CAT
    3. `filtering_base_genes_nonCAT_consistent`: aggregate information about
       consistent junctions to consistent genes per non-CAT
    4. `filtering_base_genes_CAT_nonrepresented_pairwise`: aggregate
       information about consistent and nonrepresented junctions to
       nonrepresented genes
6. [`filtering_junctions.snake`](rules/filtering_junctions.snake) (per
   comparison of CATs vs non-CATs)
    1. `filtering_junctions_nonCAT_consistent_annotated`: annotate consistent
       junctions/introns per non-CAT using information about non-represented
       CATs
    2. `filtering_junctions_nonCAT_consistent_summary_per_tissue`: provide
       summary of stratified counts for consistent junctions in different
       categories (e.g. constitutive, number of nonrepresented CATs, etc.)
    3. `filtering_junctions_nonCAT_consistent_summary`: concatenate summaries
       for all non-CATs
    4. `filtering_junctions_CAT_nonrepresented`: identify nonrepresented
       junctions per CAT across all non-CATs
    5. `filtering_junctions_CAT_summary`: Provide summary of nonrepresented
       junctions/introns over CATs with respect to all non-CATs in the
       comparison
7. [`filtering_genes.snake`](rules/filtering_genes.snake) (per comparison of
   CATs vs non-CATs)
    1. `filtering_genes_nonCAT_consistent_annotate`: equivalent of similarly
       named rule for junctions/introns
    2. `filtering_genes_nonCAT_consistent_summary_per_tissue`: equivalent of
       similarly named rule for junctions/introns
    3. `filtering_genes_nonCAT_consistent_summary`: equivalent of similarly
       named rule for junctions/introns
    4. `filtering_genes_CAT_nonrepresented`: equivalent of similarly named rule
       for junctions/introns
8. [`plots.snake`](rules/plots.snake): rules to automatically generate output
   plots using previous rules
9. [`report.snake`](rules/report.snake): rules to report relevant statistics
   using previous rules
10. [`analyze_unions.snake`](rules/analyze_unions.snake): rules to create
   tables evaluating how unions of CATs perform in addition to just one CAT at
   a time
11. [`tables.snake`](rules/tables.snake): rules to create other tables using
   previous rules


## Cluster execution

Please see
<https://snakemake.readthedocs.io/en/stable/executable.html#cluster-execution>
for details on how you can run this pipeline on various cluster environments.


## Files

+ `README.md`: README you are reading now.
+ `environment.yaml`: conda environment configuration to perform analysis
  pipeline
+ `samples.snake`: Snakemake pipeline definition for selecting samples for
  analysis
+ `mock.snake`: for use with `config/resources_mock.yaml` for creating mock
  input resources for testing workflow using `--dryrun` if actual input files
  are unavailable.
+ `analysis.snake`: Snakemake pipeline definition for performing analysis on
  aligned samples usinq MAJIQ and other downstream tools
+ `rules/`: modular sets of rules that are included in `analysis.snake` for
  different parts of the analysis
    - `*.snake`: rule definitions
    - `envs/`: conda environments used by the rules
        - `numeric.yaml`: conda environment configuration with numpy, pandas,
          and cyvcf2 for parsing VCF files
    - `scripts/`: scripts used by the rules
    - `majiq-snakemake/`: folder with included rules, environments, and scripts
      for using MAJIQ
+ `config/`: shared configuration for pipelines developed for this project.
  Default `default.yaml` has parameters that can be partially overriden by an
  additional configuration using `--configfile` flag. Includes a mock and pilot
  configuration for testing:
    - `default.yaml`: parameters for analysis given resources that can be added
      to this configuration using `--configfile resources_{setting}.yaml`
    - `resources_mock.yaml`: resource parameters for mock setting. Empty/mock
      alignments/transcript quantifications can be generated using `mock.snake`
      given the output of `samples.snake`
+ `schemas/`: Schemas detailing default and required parameters to define the
  analysis
+ `data/`: folder with input data/metadata
    - `GTExRunTable.txt`: SRA table with samples from GTEx used for random
      sampling
    - `FetalHeartAERunTable.sdrf.txt`: ArrayExpress run table with samples from
      E-MTAB-7031 for fetal heart RNA-seq
    - `HDBRFetalBrainAERunTable.sdrf.txt`: ArrayExpress run table with samples
      from E-MTAB-4840 for fetal brain RNA-seq
    - `clinvar_gene_diseases.txt`: Table downloaded from ClinVar FTP site
      detailing gene/disease relationships from different sources. Downloaded
      on May 2, 2019 by running the command:
      `curl ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/gene_condition_source_id -o clinvar_gene_diseases.txt`
    - `clingen_gene_diseases.txt`: Table of curated ClinGen gene-disease
      relationships downloaded on May 2, 2019 from
      <https://search.clinicalgenome.org/kb/gene-validity.csv>. Header and
      format modified for easier parsing
    - `clingen_hcm_panel.txt`: Table of curated ClinGen gene-disease
      relationships compiled from tables in Ingles et al 2019
    - `hpo_genes_hp_0001249.txt`: Table of genes associated with HPO term
      "HP:0001249" downloaded on May 2, 2019 from
      <https://hpo.jax.org/app/browse/term/HP:0001249>
    - `DDG2P_2_5_2019.csv`: DDD gene 2 phenotype panel downloaded from
      <https://www.ebi.ac.uk/gene2phenotype/downloads> on May 2, 2019
    - `protected/`: folder with protected input data/metadata that will not be
      tracked by git (GTEx individual-level sample/donor metadata)
+ `scripts/`: folder with Python definitions shared by the top level Snakemake
  pipelines

### Output directories

The pipelines will create additional directories that are not tracked by the
Git repository. These include:

+ `samples/`: Tables detailing selected samples from GTEx and HDBR, files for
  use with MAJIQ
+ `output/`: Files output by analysis pipeline
    - `logs/`: Log files produced by pipelines
    - `tissue_expression`: Summaries of gene expression per tissue
    - `majiq/`: Files for/produced by MAJIQ
    - `voila/`: Files produced by VOILA or associated scripts
    - `filtering/`: Files produced by filtering steps over CATs and non-CATs,
      processing output from MAJIQ/VOILA
    - `plots/`: Plots showing the results of the analysis
    - `report/`: Report of specific statistics from the analysis
    - `tables/`: Nicely formatted tables of results from the analysis
