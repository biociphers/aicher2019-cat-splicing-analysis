"""
workflow_helper.py

Python definitions that are shared between samples/analysis workflows.

Constants:
+ MAJIQ_SAMPLES, MAJIQ_QUANT_GROUPS: paths for majiq pipeline tables
Implements:

Functions:
+ to_majiq_samples_table(df, sample, build_group, **extra_columns): obtains
  table appropriate for samples in majiq-snakemake from an arbitrary table
+ to_majiq_quant_groups_table(df, *group_columns): obtains quant_groups from
  majiq samples table
+ shuffle_table(df, unique_columns, stratify_columns, random_seed): shuffles
  table, dropping duplicates in unique_columns, stratified by stratify_columns
+ fix_group_name(): regularizes names that have patterns that are not nice for
  working in filesystems (and are expected to not be present downstream)
"""

import pandas as pd
import re
from hashlib import sha256


# define constants
MAJIQ_SAMPLES = "samples/majiq_samples.tsv"
MAJIQ_QUANT_GROUPS = "samples/majiq_quant_groups.tsv"


def collect_tissues(tissues_collection, include, exclude=list()):
    """ Given list of possible tissues, include/exclude them and return list

    Given collection of possible tissues, include/exclude tissues from include
    and exclude (empty include implicitly means all tissues) and return a
    sorted list of unique tissues

    Parameters
    ----------
    tissues_collection: Collection[str]
    include: Collection[str]
    exclude: Collection[str]

    Returns
    -------
    List[str]
    """
    valid_set = set(tissues_collection)  # get unique valid tissues
    # get set that we want to include (after removing exclude)
    if include:
        include_set = set(include) - set(exclude)
    else:
        # if include is empty, it implicitly means use all valid tissues
        include_set = valid_set - set(exclude)
    # make sure that there aren't any invalid tissues
    invalid_set = include_set - valid_set
    if len(invalid_set):
        raise ValueError(
            f"Invalid tissues {invalid_set} requested for inclusion in"
            f" set {include_set} not found in {valid_set}"
        )
    # return sorted list of collected tissues to include
    return sorted(include_set)


def correct_fibroblasts(x):
    """ Corrects naming of fibroblasts (v8 release noted this issue)

    Corrects naming of fibroblasts. GTEx v1-v7 named their fibroblasts
    "Transformed fibroblasts" although they were actually just cultured. With
    the v8 release, GTEx issued a correction. This function implements this
    change without modifying the original table we are using, allowing us to
    perform the same sampling as before.
    """
    x = re.sub("Transformed fibroblast", "Cultured fibroblast", x)
    return x


# define "fixing" of group names for better filesystem use
def fix_group_name(x, first_time=True):
    """ Removes or replaces common special characters from input string

    Parameters
    ----------
    x: Union[str, Sequence[str]]
        Input body site being cleaned. Applied element-wise if fails, tries
        assuming that it is a sequence of strings
    first_time: bool
        prevent infinite recursion

    Returns
    -------
    str: mutated group string with special characters removed/replaced

    Notes
    -----
    The main characters we are changing are:
    1. parentheses -- remove these altogether
    2. consecutive spaces/dashes/slashes/underscores/equals: replace by dash

    This happens after we fix transformed fibroblasts to cultured fibroblasts.
    """
    try:
        x = correct_fibroblasts(x)  # correct fibroblast names
        x = re.sub("[()]", "", x)  # remove parentheses
        x = re.sub("[- _/=]+", "-", x)  # replace special characters
        return x
    except TypeError:
        if not first_time:
            raise
        # assume that it was an iterable of strings this time
        return [fix_group_name(y, first_time=False) for y in x]


def to_majiq_samples_table(df, sample, build_group, **extra_columns):
    """ Produces majiq table with build groups for majiq-snakemake from table

    Parameters
    ----------
    df: pd.DataFrame
    sample: str
        Column label in df that will be turned into sample column
    build_group: str
        Column label in df that will be turned into build group column, after
        fixing group name
    extra_columns: Dict[str, str]
        Keys correspond to columns in the resulting table, values to columns
        in the original table (`{new_label: old_label}`)

    Returns
    -------
    pd.DataFrame with columns: sample, build_group, and additional others.
    `build_group` is formatted nicely
    """
    # Let's create a new dataframe with one column -- sample
    result = df.rename(
        {
            sample: "sample"
        },
        axis="columns"
    )[["sample"]]  # only the sample column -- add everything back in later
    # let's get the build group, fixing the names
    result["build_group"] = df[build_group].apply(fix_group_name)
    # let's get all the extra columns
    result = result.assign(
        # add new columns to result
        **{
            # unpack extra_columns parameter, accessing old columns...
            new_label: df[old_label]
            for new_label, old_label in extra_columns.items()
        }
    )
    # return the resulting table
    return result


def to_majiq_quant_groups_table(majiq_samples, *quant_groups):
    """ Produces majiq quant_groups table from columns of majiq_samples table

    Produces majiq quant_groups table from columns of majiq_samples table.
    Reads column labels from `quant_groups` and uses the values from them to
    define quantification groups. This assumes that there is no overlap between
    unique values per column.

    Parameters
    ----------
    majiq_samples: pd.DataFrame
        DataFrame with columns sample, build_group, and other columns
    quant_groups: List[str]
        Column labels in `majiq_samples` for which the unique levels define
        quantification groups

    Returns
    -------
    pd.DataFrame with columns: sample, group

    Notes
    -----
    Raises RuntimeError if the different columns share levels that could lead
    to messy definition of groups for quantification.
    """
    # only use unique values of quant_groups
    quant_groups = set(quant_groups)
    # original samples
    samples = majiq_samples["sample"]
    # make a copy of the majiq_samples dataframe
    majiq_samples = majiq_samples.copy()
    # fix the names of columns for quant_groups
    for x in quant_groups:
        majiq_samples[x] = majiq_samples[x].apply(fix_group_name)
    # check that the levels of each column are unique
    quant_levels = [majiq_samples[x].unique() for x in quant_groups]
    union_length = len(set.union(*(set(x) for x in quant_levels)))
    sum_length = sum(len(x) for x in quant_levels)
    if union_length < sum_length:
        # there is overlap...
        raise RuntimeError("The columns for quantification groups overlap")
    # otherwise, return resulting dataframe, concatenating per column
    return pd.concat(
        [
            majiq_samples.assign(
                sample=samples,
                group=majiq_samples[x]  # the fixed quant group
            )[["sample", "group"]]
            for x in quant_groups
        ],
        join="inner"
    )


def shuffle_table(df, unique_columns, stratify_columns, random_seed):
    """ Returns a shuffled version of the input table

    Parameters
    ----------
    df: pd.DataFrame
        DataFrame to shuffle
    unique_columns: column list or column label
        for df that will be used to drop duplicates (keeping first -- reorder
        beforehand if necessary)
    stratify_columns: columns list or column label
        columns that will be used to do separate sampling with different random
        seeds (determined as function of the values)
    random_seed: int
        random seed for reproducibility
    """
    return df.drop_duplicates(
        # drop duplicates in the unique_columns
        subset=unique_columns, keep="first"
    ).groupby(
        # stratify by columns before shuffling
        stratify_columns
    ).apply(
        # shuffle the rows per subset
        lambda x: x.sample(
            # get all the rows back, just in shuffled order
            len(x),
            # define the random state as a function of the seed and group
            random_state=(
                random_seed
                +
                _shuffle_hash(x.iloc[0][stratify_columns])
            ) % (2 ** 32)  # make sure it's representable in 32 bits
        )
    ).reset_index(drop=True)


def _shuffle_hash(x):
    """ Returns a hash of the stratification group

    Parameters
    ----------
    x
        Series (from multiple values) or single value that will be hashed. If
        series, will be done in sorted index order
    """
    # make x a string
    if isinstance(x, pd.Series):
        x = "_".join(str(x[i]) for i in sorted(x.index))
    else:
        x = str(x)
    # encode it, perform sha256 hash and get back integer value
    return int(sha256(x.encode()).hexdigest(), 16)
