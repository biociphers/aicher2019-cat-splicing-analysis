#!/usr/bin/env bash -l
# submit_cluster.bash
# Submits the pipeline, assuming that there exists a profile with your cluster
# parameters. For example, github.com/jaicher/snakemake-sync-bq-sub
# By default, looks for the default profile from the above repository
# Override by setting the variable $SNAKEMAKE_PROFILE beforehand
SNAKEMAKE_PROFILE=${SNAKEMAKE_PROFILE:-"cluster-qsub"}
# Also, by default use provided cluster configuration, can be overriden
CLUSTER_CONFIG=${CLUSTER_CONFIG:-"cluster/cluster_config.yaml"}

# use bash strict mode
set -euo pipefail

# set up conda
CONDA_BASE=$(conda info --base)
. "${CONDA_BASE}/etc/profile.d/conda.sh"  # make sure we can run conda activate
conda activate cat-paper-2019-final  # activate environment

# run the pipeline, passing in additional parameters
snakemake --profile $SNAKEMAKE_PROFILE --cluster-config $CLUSTER_CONFIG "$@"

# make comment that successfully completed
echo "Successfully completed"
